﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Data", menuName = "BackgroundsList", order = 3)]
public class BackgroundsListData : ScriptableObject
{
    public List<Background> backgrounds;
}