﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Data", menuName = "Texts", order = 4)]
public class TextsScrObj : ScriptableObject
{
    public List<string> texts;

    public string GetRandomText()
    {
        return texts[Random.Range(0, texts.Count)];
    }
}
