﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Data", menuName = "ItemsList", order = 1)]
public class ItemsListData : ScriptableObject
{
    public List<Item> items;
}