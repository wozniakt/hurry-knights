﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "EnemiesListData", order = 7)]
public class EnemiesListData : ScriptableObject
{
    
    public List<GameObject> EnemiesPrefabs;


    public GameObject GetRandomEnemy()
    {
       
        GameObject rndEnemy = EnemiesPrefabs[UnityEngine.Random.Range(0, EnemiesPrefabs.Count)];
        //Debug.Log(rndEnemy.tag +"  "+ FactorySingletons.Instance.GameStateMan.SpecialActive);
        if (FactorySingletons.Instance.GameStateMan.SpecialActive == true && rndEnemy.CompareTag("Special")==true)
        {
            rndEnemy = GetRandomEnemy();
        }
        return rndEnemy;
    }

}
