﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "SponsorsList", order = 10)]
public class SponsorsList : ScriptableObject
{

    public List<SponsorsListData> SponsorsListData;

}

