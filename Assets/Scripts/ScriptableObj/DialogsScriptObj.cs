﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
[CreateAssetMenu(fileName = "Dialogs", menuName = "Dialogs", order = 10)]
public class DialogsScriptObj : ScriptableObject
{
    // Dialogs[0] - tutorial
    //Dialogs[1] - repeatable
    //Dialogs[1....] - repeat until not used or Dialogs[1]
    public List<Dialog> Dialogs;
    Dialog rndDialog;
    int counterRepeats = 0;
    void Start()
    {

    }
    void RefreshDialogs()
    {
        Debug.Log("refresh dialogs");
        foreach (Dialog dialog in Dialogs)
        {
            dialog.alreadyUsed = false;
        }
    }
    public Dialog GetRandomDialog()
    {
     
        rndDialog = Dialogs[UnityEngine.Random.Range(0, Dialogs.Count)];
        //Debug.Log("rndDialog.alreadyUsed: "+ rndDialog.alreadyUsed + "rndDialog.index: "+ Dialogs.IndexOf(rndDialog));
        if (rndDialog.alreadyUsed==true )//&& Dialogs.IndexOf(rndDialog)!=0)
        {
            counterRepeats++;
            if (counterRepeats >= Dialogs.Count)
            {
                RefreshDialogs();
            }
            rndDialog = GetRandomDialog();
        }
        rndDialog.alreadyUsed= true;
        return rndDialog;
    }
}
