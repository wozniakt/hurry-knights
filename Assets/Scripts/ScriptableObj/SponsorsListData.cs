﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[Serializable]
[CreateAssetMenu(fileName = "Data", menuName = "SponsorsListData", order = 2)]
public class SponsorsListData : ScriptableObject
{
    public List<Hero> Heroes;
    public string SponsorName;
    public Sprite SpriteImg;
    public Special Special;
    public DialogsScriptObj dialogsScriptObj;
    public DialogsScriptObj initialDialogsScriptObj;
    public DialogsScriptObj tutorialDialogsScriptObj;
}