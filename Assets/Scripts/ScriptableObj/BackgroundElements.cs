﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Data", menuName = "BackgroundElements", order = 5)]
public class BackgroundElements : ScriptableObject
{
    public List<Texture> backgroundTextures;
}
