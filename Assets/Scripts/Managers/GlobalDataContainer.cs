﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GlobalDataContainer : MonoBehaviour
{
    public List<Special> BasicSpecialsPrefabs;
    public SponsorsList sponsors;
    public ItemsListData ItemsListData;
    public BackgroundsListData BackgroundsListData;
    public SponsorsListData CurrentHeroesListData;

    public List<Item> UnlockedItems;
    public List<Background> UnlockedBackgrounds;
    public List<Hero> UnlockedHeroes;
    public List<Special> UnlockedSpecials;
    bool allHeroesUnlockedForSponsor;
    private void Start()
    {
       // CurrentHeroesListData = sponsors.SponsorsListData[0];
        GlobalEventsManager.instance.StartListening("CurrentHeroesListUpdated", OnSponsorUpdate);

    }
    void OnSponsorUpdate(Hashtable eventParams)
    {
        SponsorsListData CurrentHeroesList = (SponsorsListData)eventParams["CurrentHeroesListData"];
        PrepareAvailableItems(CurrentHeroesList);
        GetAvailableSpecials();
    }

    void GetAvailableSpecials()
    {
        foreach (SponsorsListData sponsor in sponsors.SponsorsListData)
        {
            sponsor.Special.Unlocked = false;
            if (sponsor.Special.unlockValue <= FactorySingletons.Instance.PlayerDataManager.CurrentPlayerData.HiscorePoints)
            {
                sponsor.Special.Unlocked = true;
                UnlockedSpecials.Add(sponsor.Special);
            }
        }

        UnlockedSpecials.AddRange(BasicSpecialsPrefabs);
    }


    public void PrepareAvailableItems() {
        //todo
        //UnlockedCarts = new List<Hero>();
        //foreach (Hero hero in CurrentHeroesListData.Heroes)
        //{
        //    if (hero.unlockValue <= FactorySingletons.Instance.PlayerDataManager.CurrentPlayerData.HiscorePoints)
        //    {
        //        hero.Unlocked = true;
        //        UnlockedCarts.Add(hero);
        //    }
        //    else
        //    {
        //        hero.Unlocked = false;
        //    }
        //}
    }
    public  void PrepareAvailableItems(SponsorsListData currentHeroesList)
    {
        //int maxItemIndex = 1 + FactorySingletons.Instance.PlayerDataManager.CurrentPlayerData.HiscorePoints / FactorySingletons.Instance.PlayerDataManager.PointsDivider;
        UnlockedSpecials = new List<Special>();
  
        UnlockedItems = new List<Item>();
        foreach (Item item in ItemsListData.items)
        {
            if (item.unlockValue <= FactorySingletons.Instance.PlayerDataManager.CurrentPlayerData.HiscorePoints)
            {
                item.Unlocked = true;
                UnlockedItems.Add(item);
            }
            else
            {
                item.Unlocked = false;
            }
        }

        //UnlockedBackgrounds = new List<Background>();
        //foreach (Background bg in BackgroundsListData.backgrounds)
        //{
        //    if (bg.unlockValue <= FactorySingletons.Instance.PlayerDataManager.CurrentPlayerData.HiscorePoints)
        //    {
        //        bg.Unlocked = true;
        //        UnlockedBackgrounds.Add(bg);
        //    }
        //    else
        //    {
        //        bg.Unlocked = false;
        //    }
        //}
        UnlockedHeroes = new List<Hero>();
        foreach (Hero hero in currentHeroesList.Heroes)
        {
            if (hero.UnlockValue <= FactorySingletons.Instance.PlayerDataManager.CurrentPlayerData.HiscorePoints)
            {
                hero.Unlocked = true;
                UnlockedHeroes.Add(hero);
                if (hero.Special.prefab != null)
                {
                    UnlockedSpecials.Add(hero.Special);
                }

            }
            else
            {
                hero.Unlocked = false;
            }
        }
    }
}
