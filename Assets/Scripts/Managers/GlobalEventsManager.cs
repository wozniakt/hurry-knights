﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using System.Collections.Generic;

public class GlobalEventsManager : MonoBehaviour
{

    public class CustomUnityEvent : UnityEvent<Hashtable> { }
    private Dictionary<string, CustomUnityEvent> eventDictionary;

    private static GlobalEventsManager eventManager;

    public static GlobalEventsManager instance
    {
        get
        {
            if (!eventManager)
            {
                eventManager = FindObjectOfType(typeof(GlobalEventsManager)) as GlobalEventsManager;

                if (!eventManager)
                {
                    Debug.LogError("There needs to be one active EventManger script on a GameObject in your scene.");
                }
                else
                {
                    eventManager.Init();
                }
            }

            return eventManager;
        }
    }
    void Awake()
    {
        Init();
    }

    public void Init()
    {
        if (eventDictionary == null)
        {
            eventDictionary = new Dictionary<string, CustomUnityEvent>();
        }
    }

    public void StartListening(string eventName, UnityAction<Hashtable> listener)
    {
        CustomUnityEvent thisEvent = null;
        if (eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.AddListener(listener);
        }
        else
        {
            thisEvent = new CustomUnityEvent();
            thisEvent.AddListener(listener);
            eventDictionary.Add(eventName, thisEvent);
        }
    }

    public void StopListening(string eventName, UnityAction<Hashtable> listener)
    {
        //		if (eventManager == null) return;
        CustomUnityEvent thisEvent = null;
        if (eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.RemoveListener(listener);
        }
    }

    public void TriggerEvent(string eventName, Hashtable eventParams = default(Hashtable))
    {
        CustomUnityEvent thisEvent = null;
        if (eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.Invoke(eventParams);
        }
    }

    public void TriggerEvent(string eventName)
    {
        TriggerEvent(eventName, null);
    }
}

