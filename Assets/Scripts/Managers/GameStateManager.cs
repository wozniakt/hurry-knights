﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameStateManager : MonoBehaviour
{
    public enum GameState { Menu, Shop, GameOn, Win, Lost, RestartGame, ShopWorkaround , BossActive, BossAttackCheck}
    public bool DialogOn=false;
    public bool DialogTutorialOn = false;
    public bool DialogInitialOn = false;
    public bool StartSpecialDialog = false;
    public GameState CurrentGameState;
    public bool SpecialActive=false;
    public int CurrentCartsCounter, CurrentItemsCounter;
    public float BackgroundSpeed;

    private void Start()
    {
        ChangeGameState( GameState.Menu);
        GlobalEventsManager.instance.StartListening("CountersChanged",OnCountersChange);
    }
    void OnCountersChange(Hashtable eventParams)
    {
        if (eventParams.ContainsKey("CartsCounter"))
        {
            int cartsCounter = (int)eventParams["CartsCounter"];
            CurrentCartsCounter += cartsCounter;
            GlobalEventsManager.instance.TriggerEvent("ChangeSpawningStateCarts", new Hashtable { {"CartsSpawning", CurrentCartsCounter==0 } });
        }
        if (eventParams.ContainsKey("ItemsCounter"))
        {
            int itemsCounter = (int)eventParams["ItemsCounter"];
            CurrentItemsCounter += itemsCounter;
            if (CurrentItemsCounter==0 && FactorySingletons.Instance.PlayerDataManager.CurrentPlayerData.CurrentHp > 0)
            {
                ChangeGameState(GameState.Win);
            }
        }
    }

    public void ChangeGameState(GameState gameState)
    {
        CurrentGameState = gameState;

        switch (gameState)
        {
            case (GameState.Menu):
                FactorySingletons.Instance.GlobalDataContainer.PrepareAvailableItems();
                break;
            case (GameState.GameOn):
         
                break;
            case (GameState.BossActive):
                GlobalEventsManager.instance.TriggerEvent("BossActive");
                break;
            case (GameState.Win):
               
                break;
            case (GameState.Lost):
                AllHeroesDie();
                break;

            case (GameState.RestartGame):
               // LoadGameScene("CrazyRunScene");
                
                break;
        }
        GlobalEventsManager.instance.TriggerEvent("GameStateChanged", new Hashtable { { "GameState", gameState } });

    }

    IEnumerator BossWin()
    {
        Debug.Log("Boss win coroutimne 1");
        yield return new WaitForSeconds(1f);
        Debug.Log("Boss win coroutimne 2");
    }

    public void LoadGameScene(GameState gameStateAfterLoadingScene)
    {
        FactorySingletons.Instance.GlobalDataContainer.PrepareAvailableItems();
        StartCoroutine(LoadAsyncScene("CrazyRunScene", gameStateAfterLoadingScene));
    }

    void AllHeroesDie()
    {
        GameObject[] heroes = GameObject.FindGameObjectsWithTag("Hero");
        foreach (GameObject hero in heroes)
        {
            hero.GetComponent<IHealth>().DecreaseHealth(100);
        }
    }

    IEnumerator LoadAsyncScene(string sceneName, GameState gameStateAfterLoadingScene)
    {
        FactorySingletons.Instance.PlayerDataManager.SavePlayerData();
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneName);
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
        ChangeGameState(gameStateAfterLoadingScene);
        //FactorySingletons.Instance.PlayerDataManager.LoadPlayerData();
        //		ChangeStateGame (GameState.GameIsOn);
    }
}
