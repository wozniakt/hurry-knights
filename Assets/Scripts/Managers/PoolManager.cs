﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class PoolManager : MonoBehaviour
{

    //public static string POOLED_PREFAB_PATH = "Prefabs/";
    //public static PoolManager instance;
     GameObject GoInstancesPool;
    public int pooledAmount = 10;
    public bool WillGrow = true;
    //public List<GameObject>  pooledCarts= new List<GameObject>();
    //public List<GameObject> pooledItems = new List<GameObject>();
    public List<GameObject> poolList = new List<GameObject>();
    private void Awake()
    {
        GoInstancesPool = new GameObject();
        GoInstancesPool.transform.SetParent(this.transform);
       // GoInstancesPool.AddComponent<MoveItemsContainerComponent>();
        //poolList = new List<GameObject>();
    }
    public void ResetPooledList()
    {
        poolList = new List<GameObject>();
        Destroy(GoInstancesPool);
        GoInstancesPool = new GameObject();
       // GoInstancesPool.AddComponent<MoveItemsContainerComponent>();
        GoInstancesPool.transform.SetParent(this.transform);
    }

    public void createGoInstances(int count, GameObject GoInstancePrefab)
    {
        for (int i = 0; i < count; i++)
        {
            GameObject obj = (GameObject)Instantiate(GoInstancePrefab);

            obj.SetActive(false);
            obj.name = GoInstancePrefab.name.ToString();
            poolList.Add(obj);
            obj.transform.SetParent(GoInstancesPool.transform);
        }

    }
    
    //
    public GameObject SpawnGoInstance(GameObject GoInstancePrefab)
    {
        foreach (GameObject item in poolList)
        {

            if (item.name == GoInstancePrefab.name.ToString())
            {
                if (!item.activeInHierarchy)
                {
                    return item;
                }
                else
                {

                }
            }
        }

        if (WillGrow)
        {
            GameObject obj = (GameObject)Instantiate(GoInstancePrefab);
            obj.name = GoInstancePrefab.name.ToString();
            poolList.Add(obj);
            obj.transform.SetParent(GoInstancesPool.transform);

            return obj;


        }
        return null;
    }

}