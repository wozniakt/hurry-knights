﻿using UnityEngine;
using System.Collections;

public class FactorySingletons : SingletonCustom<FactorySingletons>
{
	public static FactorySingletons Instance;
	public PoolManager PoolManager;
    public GameStateManager GameStateMan;
    public PlayerDataManager PlayerDataManager;
    public GlobalDataContainer GlobalDataContainer;
    void Awake()
    {

        if (Instance == null)
        {

            Instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

}

