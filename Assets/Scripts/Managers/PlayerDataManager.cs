﻿using UnityEngine;
using System.Collections;

public class PlayerDataManager : MonoBehaviour
{
    public SponsorsList SponsorsListData;
    public PlayerData CurrentPlayerData = new PlayerData();
    public int PointsDivider=50;

    // Use this for initialization
    void Start()
    {
       // PlayerPrefs.SetInt("ShowTutorial", 1);
        GlobalEventsManager.instance.StartListening("GivePoints", OnPointsChange);
        GlobalEventsManager.instance.StartListening("PlayerHpChanged", OnPlayerHpChangedChange);

        CurrentPlayerData.CurrentHp = CurrentPlayerData.MaxHp;
        CurrentPlayerData.CurrentSponsorListData = FactorySingletons.Instance.GlobalDataContainer.sponsors.SponsorsListData[0];
        LoadPlayerData();
      //  CurrentPlayerData.CurrentBackground = CurrentPlayerData.CurrentHero.BackgroundElements.backgroundSprites[0];

        
    }
    
    // Update is called once per frame
    void OnPointsChange(Hashtable eventParams)
    {
        int points = (int)eventParams["Points"];
        CurrentPlayerData.CurrentPoints = CurrentPlayerData.CurrentPoints + points;
        SavePlayerData();
        GlobalEventsManager.instance.TriggerEvent("PlayerDataChanged", new Hashtable { { "PlayerData", CurrentPlayerData } });
    }

    void OnPlayerHpChangedChange(Hashtable eventParams)
    {
        int hpPlayerDamage = (int)eventParams["PlayerHp"];
        CurrentPlayerData.CurrentHp = CurrentPlayerData.CurrentHp - hpPlayerDamage;
        GlobalEventsManager.instance.TriggerEvent("PlayerDataChanged", new Hashtable { { "PlayerData", CurrentPlayerData } });
        if (CurrentPlayerData.CurrentHp<=0)
        {
            FactorySingletons.Instance.GameStateMan.ChangeGameState(GameStateManager.GameState.Lost);
        }
    }

   public void LoadPlayerData()
    {
        if (PlayerPrefs.GetInt("ShowTutorial",1) == 1)
        {
            CurrentPlayerData.ShowTutorial = true;
        }
        else
        {
            CurrentPlayerData.ShowTutorial = false;
        }
       // CurrentPlayerData.CurrentPoints = 0;
        CurrentPlayerData.HiscorePoints = PlayerPrefs.GetInt("HiscorePoints", 0);

        CurrentPlayerData.CurrentHeroIndex =Mathf.Max( PlayerPrefs.GetInt("CurrentHeroIndex", 0),0);
        CurrentPlayerData.CurrentSponsorIndex = 0;//=Mathf.Max( PlayerPrefs.GetInt("CurrentSponsorIndex", 0),0);
        CurrentPlayerData.CurrentHero = FactorySingletons.Instance.GlobalDataContainer.sponsors.SponsorsListData[CurrentPlayerData.CurrentSponsorIndex].Heroes[CurrentPlayerData.CurrentHeroIndex];
        CurrentPlayerData.CurrentSponsorListData= FactorySingletons.Instance.GlobalDataContainer.sponsors.SponsorsListData[CurrentPlayerData.CurrentSponsorIndex];
        CurrentPlayerData.CurrentLevel = PlayerPrefs.GetInt("CurrentLevel", 1);
        CurrentPlayerData.CurrentBgIndex=PlayerPrefs.GetInt("CurrentBgIndex",0);
        GlobalEventsManager.instance.TriggerEvent("UpdateBackground", new Hashtable { { "BackgroundElementsIndex", CurrentPlayerData.CurrentBgIndex } });

        GlobalEventsManager.instance.TriggerEvent("PlayerDataChanged", new Hashtable { { "PlayerData", CurrentPlayerData } });
  
        if (CurrentPlayerData.CurrentHeroesList==null || CurrentPlayerData.CurrentHeroesList.Heroes.Count==0)
        {
            CurrentPlayerData.CurrentHeroesList = SponsorsListData.SponsorsListData[0];
        }
        GlobalEventsManager.instance.TriggerEvent("CurrentHeroesListUpdated", new Hashtable { { "CurrentHeroesListData", CurrentPlayerData.CurrentHeroesList } });
    }

    public void ResetPlayerHp()
    {
        CurrentPlayerData.CurrentHp = CurrentPlayerData.MaxHp;
    }

   public void SavePlayerData()
    {

        {
            CurrentPlayerData.HiscorePoints =Mathf.Max( CurrentPlayerData.CurrentPoints, CurrentPlayerData.HiscorePoints);
            PlayerPrefs.SetInt("HiscorePoints", CurrentPlayerData.HiscorePoints);
            if (CurrentPlayerData.ShowTutorial==true)
            {
                PlayerPrefs.SetInt("ShowTutorial", 1);
            }
            else
            {
                PlayerPrefs.SetInt("ShowTutorial", 0);
            }

           PlayerPrefs.SetInt("CurrentLevel", CurrentPlayerData.CurrentLevel);
            CurrentPlayerData.CurrentSponsorIndex = 0;// FactorySingletons.Instance.GlobalDataContainer.sponsors.SponsorsListData.IndexOf(CurrentPlayerData.CurrentSponsorListData);
            CurrentPlayerData.CurrentHeroIndex = FactorySingletons.Instance.GlobalDataContainer.sponsors.SponsorsListData[CurrentPlayerData.CurrentSponsorIndex].Heroes.IndexOf(CurrentPlayerData.CurrentHero);

            PlayerPrefs.SetInt("CurrentHeroIndex",CurrentPlayerData.CurrentHeroIndex);
            PlayerPrefs.SetInt("CurrentSponsorIndex",CurrentPlayerData.CurrentSponsorIndex);

        }
    }
}
