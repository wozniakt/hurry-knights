﻿using UnityEngine;
using System.Collections;

public class EnemyBossActivation : MonoBehaviour
{
    float timerForStartAttack = 0;
    // Use this for initialization
    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.CompareTag("Hero"))
        {
          
            FactorySingletons.Instance.GameStateMan.CurrentGameState = GameStateManager.GameState.BossActive;
            GlobalEventsManager.instance.TriggerEvent("BossActivePreparation");
            GlobalEventsManager.instance.TriggerEvent("BossActive");
        }

     
        if (col.gameObject.CompareTag("HeroAttack") && FactorySingletons.Instance.GameStateMan.CurrentGameState == GameStateManager.GameState.Lost)
        {
           
            //GetComponent<IDeath>().Die();
            GetComponent<IHealth>().DecreaseHealth(1);
        }
    }

    private void OnTriggerStay2D(Collider2D collider)
    {
      
        if (collider.CompareTag("HeroAttack") &&   FactorySingletons.Instance.GameStateMan.CurrentGameState == GameStateManager.GameState.Win)
        {
       
            //GetComponent<IDeath>().Die();
            GetComponent<IHealth>().DecreaseHealth(1);
        }
    }
}
