﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] GameObject bossPrefab;

    public enum Direction { Left, Right };
    [SerializeField] float cartSpeed;
    [SerializeField] Direction startingDirection;
    // GameObject enemyPrefab;

    public bool isSpawning = false;
    GameObject enemyGO;
    [SerializeField] float spawnDelay = 1, randomMin, randomMax;
    [SerializeField]float timer = 0,  levelTimer, bossTime=1;
    float rndTimer;
    int bossCounter;
    int currentLevel;
 
    // Use this for initialization
    void Start()
    {
        GlobalEventsManager.instance.StartListening("GameStateChanged", OnGameStateChange);
        GlobalEventsManager.instance.StartListening("EnemyDied", OnEnemyDied);
    }
    private void OnEnable()
    {
        bossCounter = 0;
        currentLevel= FactorySingletons.Instance.PlayerDataManager.CurrentPlayerData.CurrentLevel;
    }
    void OnEnemyDied(Hashtable eventParams)
    {
        if (FactorySingletons.Instance.GameStateMan.CurrentGameState == GameStateManager.GameState.GameOn && FactorySingletons.Instance.GameStateMan.DialogOn==false)
        {
            isSpawning = true;
           // timer = 0;
        }
    }

    void OnGameStateChange(Hashtable eventParams)
    {
        GameStateManager.GameState gameState = (GameStateManager.GameState)eventParams["GameState"];
        timer = 0;
        isSpawning = (gameState == GameStateManager.GameState.GameOn);
    }
    // Update is called once per frame
    void Update()
    {

        timer += Time.deltaTime;
        levelTimer += Time.deltaTime;
        // if (timer>spawnDelay && isSpawning && FactorySingletons.Instance.GameStateMan.CurrentGameState == GameStateManager.GameState.GameOn )//&& FactorySingletons.Instance.GameStateMan.CurrentCartsCounter < 1)
       // Debug.Log("FactorySingletons.Instance.GameStateMan.CurrentGameState: " + FactorySingletons.Instance.GameStateMan.CurrentGameState);
        if (timer >= (spawnDelay+ rndTimer) && FactorySingletons.Instance.GameStateMan.CurrentGameState == GameStateManager.GameState.GameOn && FactorySingletons.Instance.GameStateMan.DialogOn == false)//&& FactorySingletons.Instance.GameStateMan.CurrentCartsCounter < 1)
        {
            float maxLevelTime = Mathf.Min(bossTime* currentLevel, 60);
            maxLevelTime = Mathf.Max(maxLevelTime, 6);
            if (levelTimer >= maxLevelTime -8 && FactorySingletons.Instance.GameStateMan.SpecialActive==false)
            {
                FactorySingletons.Instance.GameStateMan.SpecialActive = true;
            }
            if (levelTimer >= maxLevelTime  && bossCounter==0)
            {
                SpawnBoss();
                 return;
            }
            else if (levelTimer < maxLevelTime && bossCounter == 0)
            {
                SpawnEnemyNormal();
            }
           // bossCounter = 0;
           
        }

    }
    void SpawnEnemyNormal()
    {
        GameObject enemyPrefab = FactorySingletons.Instance.PlayerDataManager.CurrentPlayerData.CurrentHero.EnemiesCollection.GetRandomEnemy();
        enemyGO = FactorySingletons.Instance.PoolManager.SpawnGoInstance(enemyPrefab);

        enemyGO.SetActive(true);

        IMove moveComp = enemyGO.GetComponent<IMove>();

        moveComp.Speed = cartSpeed;

        enemyGO.transform.position = this.transform.position;
        rndTimer = Random.Range(randomMin, randomMax);
        timer = 0;
        isSpawning = false;
    }

    void SpawnBoss()
    {
        if (bossCounter>0)
        {
            return;
        }
        GlobalEventsManager.instance.TriggerEvent("BossAppears");
        GameObject enemyPrefab = bossPrefab;
        enemyGO = FactorySingletons.Instance.PoolManager.SpawnGoInstance(enemyPrefab);

        enemyGO.SetActive(true);

        IMove moveComp = enemyGO.GetComponent<IMove>();

        moveComp.Speed = cartSpeed;

        enemyGO.transform.position = this.transform.position;
        rndTimer = Random.Range(randomMin, randomMax);

        timer = 0;
        isSpawning = false;
        bossCounter++;
    }
}

