﻿using UnityEngine;
using System.Collections;

public class DisableEnemyComponent : MonoBehaviour, IDisable
{
    public void DisableGO()
    {
        this.gameObject.SetActive(false);
    }

}
