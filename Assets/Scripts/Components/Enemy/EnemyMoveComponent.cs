﻿using UnityEngine;
using System.Collections;

public class EnemyMoveComponent : MonoBehaviour, IMove
{
    public float SpeedMultipler=1;
   public float Speed { get; set; }
    bool isMoving = true;
    void FixedUpdate()
    {

        if (isMoving)
        {

            transform.Translate(-1 * Speed* SpeedMultipler * Time.deltaTime, 0, 0);
        }

    }
}
