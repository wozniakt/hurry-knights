﻿using UnityEngine;
using System.Collections;

public class EnemyDeathComponent : MonoBehaviour, IDeath
{
    [SerializeField] GameObject deathEffect;
    [SerializeField] GameObject deathEffect2;
    public void Die()
    {

        GetComponent<GivePointsComponent>().GivePoints();
        if (deathEffect!=null)
        {
            GameObject deathEffectGo = FactorySingletons.Instance.PoolManager.SpawnGoInstance(deathEffect);
            deathEffectGo.SetActive(true);
            deathEffectGo.transform.position = this.transform.position;
        }
        if (deathEffect2 != null)
        {
            GameObject deathEffectGo2 = FactorySingletons.Instance.PoolManager.SpawnGoInstance(deathEffect2);
            deathEffectGo2.SetActive(true);
            deathEffectGo2.transform.position = this.transform.position;
        }
        GetComponent<IDisable>().DisableGO();

        GlobalEventsManager.instance.TriggerEvent("EnemyDied");
    }



}
