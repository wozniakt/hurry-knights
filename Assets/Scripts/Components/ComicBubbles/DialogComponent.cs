﻿using UnityEngine;
using System.Collections;
using System.IO;

public class DialogComponent : MonoBehaviour
{
    Dialog currentDialog;
    float currentBubbleTimer = 0;
    string currentText;
    int currentTextIndex;
    float dialogsDelayTimer, dialogsDelay=5;
    //public List<Dialog> Bubbles;
    // Use this for initialization
    Jokes JokesArr;
    //[ExecuteInEditMode]
    void Start()
    {

        string filePath = Path.Combine(Application.streamingAssetsPath, "jokes.json");
        if (File.Exists(filePath))
        {
            string dataAsJson = File.ReadAllText(filePath);
            JokesArr = new Jokes();
            JokesArr.JokesArray = new Joke[] { };
            JokesArr = JsonUtility.FromJson<Jokes>(dataAsJson);
        }
        if (FactorySingletons.Instance.PlayerDataManager.CurrentPlayerData.CurrentHeroesList.dialogsScriptObj.Dialogs.Count < JokesArr.JokesArray.Length)
        {
            foreach (Joke joke in JokesArr.JokesArray)
            {
                FactorySingletons.Instance.PlayerDataManager.CurrentPlayerData.CurrentHeroesList.dialogsScriptObj.Dialogs.Add
                    (new Dialog() { ShowTime = 3, DialogTexts = new System.Collections.Generic.List<string>() { joke.setup, joke.punchline } });
            }
        }

        GlobalEventsManager.instance.StartListening ("StartTalking", ShowDialog);
    }

    // Update is called once per frame
    public void ShowDialog(Hashtable eventParams)
    {
        Dialog dialog = (Dialog)eventParams["Dialog"];

        currentDialog = dialog;
        currentText = currentDialog.DialogTexts[0];
        currentTextIndex = 0;
        dialogsDelayTimer = 0;

    }

    private void Update()
    {
        if (FactorySingletons.Instance.GameStateMan.DialogOn)
        {
            if (FactorySingletons.Instance.GameStateMan.DialogInitialOn || FactorySingletons.Instance.GameStateMan.DialogTutorialOn)
            {
                dialogsDelayTimer = dialogsDelay + 1;
            }
            dialogsDelayTimer += Time.deltaTime;
            if (dialogsDelayTimer< dialogsDelay)
            {
                return;
            }
            if (FactorySingletons.Instance.GameStateMan.CurrentGameState != GameStateManager.GameState.GameOn)
            {
                FactorySingletons.Instance.GameStateMan.DialogOn = false;
                FactorySingletons.Instance.GameStateMan.DialogTutorialOn = false;
                FactorySingletons.Instance.GameStateMan.DialogInitialOn = false;
                FactorySingletons.Instance.GameStateMan.StartSpecialDialog = false;
                GlobalEventsManager.instance.TriggerEvent("ShowBubble", new Hashtable { { "Counter", -1 }, { "Text", "" } });
                currentBubbleTimer = 0;
                return;
            }
            GlobalEventsManager.instance.TriggerEvent("ShowBubble", new Hashtable { { "Counter", currentTextIndex }, { "Text", currentText } });
            currentBubbleTimer += Time.deltaTime;


            if (currentBubbleTimer> currentDialog.ShowTime)
            {
                if (currentTextIndex +1== currentDialog.DialogTexts.Count)
                {
                    FactorySingletons.Instance.GameStateMan.DialogOn = false;
                    FactorySingletons.Instance.GameStateMan.DialogTutorialOn = false;
                    FactorySingletons.Instance.GameStateMan.DialogInitialOn = false;
                    FactorySingletons.Instance.GameStateMan.StartSpecialDialog = false;
                    GlobalEventsManager.instance.TriggerEvent("ShowBubble", new Hashtable { { "Counter", -1 }, { "Text", "" } });
                    currentBubbleTimer = 0;
                    return;
                }
                currentTextIndex++;
                currentText = currentDialog.DialogTexts[currentTextIndex];

                currentBubbleTimer = 0;
               // GlobalEventsManager.instance.TriggerEvent("ShowBubble", new Hashtable { { "Counter", currentTextIndex }, { "Text", currentText } });
    
            }
        }
    }
}
