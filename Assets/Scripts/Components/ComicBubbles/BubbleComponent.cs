﻿using UnityEngine;
using System.Collections;
using TMPro;

public class BubbleComponent : MonoBehaviour
{
    [SerializeField] GameObject textBubble;
    [SerializeField] TextMeshProUGUI textBubbleText;
    [SerializeField] bool odd;
    // Use this for initialization
    void Start()
    {
        GlobalEventsManager.instance.StartListening("ShowBubble", ShowBubble);
    }

    // Update is called once per frame
    void ShowBubble(Hashtable eventParams)
    {

        int counter = (int)eventParams["Counter"];
        textBubble.SetActive(false);
        if (counter<0 )
        {
            textBubble.SetActive(false);
            return;
        }
        if ((counter%2)==0 && odd==false)
        {
            string text = (string)eventParams["Text"];
            textBubbleText.text = text;
            textBubble.SetActive(true);
        }
        if ((counter % 2) != 0 && odd == true)
        {
            string text = (string)eventParams["Text"];
            textBubbleText.text = text;
            textBubble.SetActive(true);
        }

      
    }
}
