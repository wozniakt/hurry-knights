﻿
using UnityEngine;
using System.Collections;
using DG.Tweening;
using System;
using UnityEngine.UI;

public class TweenWinMoveTextComponent : MonoBehaviour, ITweenable
{
    Tween myTween=null;
    [SerializeField] Image imageFade;
    [SerializeField] GameObject fadeTweenGo;
    Tween fadeTween;
    [SerializeField] float duration= 0.5f;
    [SerializeField] Transform center;
    [SerializeField] Transform leftBorder;
    [SerializeField] Transform rightBorder;
    public void DoTweenAnim()
    {
        fadeTween = imageFade.DOFade(1,1);
        fadeTween.WaitForCompletion();
        StartCoroutine(TweenAnimation((x) => {
             
         }));
    }

    void ITweenable.GetDoTweenAnim(Action<bool> callback)
    {

        StartCoroutine(TweenAnimation((x)=> {

            callback ( x);
        }));
    }

    IEnumerator TweenAnimation(Action<bool> callback=null)
    {
        Debug.Log("TweenAnimation(Action<bool> callback=null) from TweenMoveTextComponent 1" );
        RectTransform textRectTransform = GetComponent<RectTransform>();
        Vector3 startPos = new Vector3( leftBorder.position.x- textRectTransform.sizeDelta.x, leftBorder.position.y, leftBorder.position.z);
        this.transform.position = startPos;
        Tween myTween = transform.DOMove(center.position, duration);
        yield return new WaitForSeconds(1f);
        GameObject[] heroes = GameObject.FindGameObjectsWithTag("Hero");
        foreach (GameObject hero in heroes)
        {
            hero.transform.DOMove(new Vector2(hero.transform.position.x + 100, hero.transform.position.y), 7);
        }
        Debug.Log("TweenAnimation(Action<bool> callback=null) from TweenMoveTextComponent 1.5");
        yield return myTween.WaitForCompletion();
        Debug.Log("TweenAnimation(Action<bool> callback=null) from TweenMoveTextComponent 2");
        yield return new WaitForSeconds(duration);
        Vector3 finalPos = new Vector3(textRectTransform.sizeDelta.x+ rightBorder.position.x, rightBorder.position.y, rightBorder.position.z);
        Tween myTween2 = transform.DOMove(finalPos, duration);
        yield return myTween2.WaitForCompletion();
        Debug.Log("TweenAnimation(Action<bool> callback=null) from TweenMoveTextComponent 3");


        fadeTweenGo.SetActive(true);
        fadeTween = imageFade.DOFade(1, 1);
        yield return fadeTween.WaitForCompletion();
        imageFade.DOFade(0, 0);
        fadeTweenGo.SetActive(false);
        callback(true);
    }

}
