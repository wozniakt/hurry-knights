﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using System;

public class TweenJumpComponent : MonoBehaviour, ITweenable
{
    Tween moveTween;
    public void DoTweenAnim()
    {
        StartCoroutine(TweenAnimation());
    }

    public Tween GetDoTweenAnim()
    {
        Tween myTween = transform.DOJump(new Vector3(transform.position.x, transform.position.y + 10, transform.position.z), 1, 1, 1);
        return myTween;
    }

    void GetDoTweenAnim(Action<bool> callback)
    {
        //StartCoroutine(TweenAnimation((x) => {
        //    callback(x);
        //}));
    }

    void ITweenable.GetDoTweenAnim(Action<bool> callback)
    {
        throw new NotImplementedException();
    }

    IEnumerator TweenAnimation()
    {
        Tween myTween = transform.DOJump(new Vector3(transform.position.x, transform.position.y + 10, transform.position.z), 1, 1, 1);
        yield return myTween.WaitForCompletion();
    }


}
