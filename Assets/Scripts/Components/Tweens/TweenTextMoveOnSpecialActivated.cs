﻿
using UnityEngine;
using System.Collections;
using DG.Tweening;
using System;
using UnityEngine.UI;

public class TweenTextMoveOnSpecialActivated : MonoBehaviour, ITweenable
{
    Tween myTween = null;
    Tween fadeTween;
    [SerializeField] float duration = 0.5f;
    [SerializeField] Transform center;
    [SerializeField] Transform leftBorder;
    [SerializeField] Transform rightBorder;
    public void DoTweenAnim()
    {


        StartCoroutine(TweenAnimation((x) => {

        }));
    }

    void ITweenable.GetDoTweenAnim(Action<bool> callback)
    {

        StartCoroutine(TweenAnimation((x) => {

            callback(x);
        }));
    }

    IEnumerator TweenAnimation(Action<bool> callback = null)
    {

        RectTransform textRectTransform = GetComponent<RectTransform>();
        Vector3 startPos = new Vector3(leftBorder.position.x - textRectTransform.sizeDelta.x, leftBorder.position.y, leftBorder.position.z);
        this.transform.position = startPos;
        Tween myTween = transform.DOMove(center.position, duration);
        yield return new WaitForSeconds(1f);
        
        yield return new WaitForSeconds(duration);
        Vector3 finalPos = new Vector3(textRectTransform.sizeDelta.x + rightBorder.position.x, rightBorder.position.y, rightBorder.position.z);
        Tween myTween2 = transform.DOMove(finalPos, duration);
        yield return myTween2.WaitForCompletion();
        callback(true);
    }

}
