﻿using UnityEngine;
using System.Collections;

public class SwitchKinematic : MonoBehaviour, IFalling
{
    Rigidbody2D rigidBody2d;
    // Use this for initialization
    void Start()
    {
        rigidBody2d = GetComponent<Rigidbody2D>();
    }

    public void StartFalling()
    {
        rigidBody2d.bodyType = RigidbodyType2D.Dynamic;

    }


}

