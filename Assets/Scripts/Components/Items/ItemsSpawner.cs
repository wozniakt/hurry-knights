﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ItemsSpawner : MonoBehaviour
{
    //[SerializeField]  List<GameObject> itemPrefabs;
    [SerializeField] ItemsListData itemsListScrObj;
    List<Item> availableItems;
    bool isSpawning=false;
    public bool active = false;
    GameObject itemGo;
    // Use this for initialization
    void Start()
    {
        GlobalEventsManager.instance.StartListening("GameStateChanged", OnGameStateChange);
    }

    void OnGameStateChange(Hashtable eventParams)
    {
        GameStateManager.GameState gameState = (GameStateManager.GameState)eventParams["GameState"];

        isSpawning =(gameState == GameStateManager.GameState.GameOn);
    }
    // Update is called once per frame
    void Update()
    {
        if (isSpawning && active)
        {
          Item rndItem = FactorySingletons.Instance.GlobalDataContainer.UnlockedItems[Random.Range(0, FactorySingletons.Instance.GlobalDataContainer.UnlockedItems.Count)];
          itemGo =  FactorySingletons.Instance.PoolManager.SpawnGoInstance(rndItem.prefab);
          itemGo.SetActive(true);
          itemGo.transform.position = this.transform.position;
          isSpawning = false;
        }

    }
}
