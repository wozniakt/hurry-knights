﻿using UnityEngine;
using System.Collections;

public class GivePointsComponent : MonoBehaviour
{
    [SerializeField] int points;
    // Use this for initialization
    public void GivePoints()
    {
        GlobalEventsManager.instance.TriggerEvent("GivePoints",new Hashtable { {"Points", points } });
    }

}
