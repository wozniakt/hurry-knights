﻿using UnityEngine;
using System.Collections;

public class DisableItemComponent : MonoBehaviour, IDisable
{

    // Use this for initialization
    public void DisableGO()
    {
        this.gameObject.SetActive(false);
    }


}
