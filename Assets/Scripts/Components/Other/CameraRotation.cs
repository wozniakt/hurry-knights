﻿

using UnityEngine;
using System.Collections;

public class CameraRotation : MonoBehaviour
{

    [SerializeField] float delayTime = 1;
    [SerializeField] Camera rotationCamera;
    float timer;
    float z;
    bool forward = true;
    private void Update()
    {

        if (FactorySingletons.Instance.GameStateMan.CurrentGameState== GameStateManager.GameState.GameOn )
        {
            timer += Time.deltaTime;

            if ( timer > delayTime)
            {
                if (forward)
                {
                    z += 0.9f;
                }
                if (z >= 90)
                {
                    forward = false;
                   
                }
                if (z <= -90)
                {
                    forward = true;

                }
                if (forward==false)
                {
                    z -= 0.9f;
                }
                rotationCamera.transform.eulerAngles = new Vector3(0, 0, z);

            }
        }
    }


}
