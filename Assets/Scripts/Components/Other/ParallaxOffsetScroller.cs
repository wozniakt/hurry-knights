﻿using UnityEngine;
using System.Collections;

public class ParallaxOffsetScroller : MonoBehaviour
{
    public float Speed=-1;
    public Renderer renderer;
    private void Start()
    {
        renderer = GetComponent<Renderer>();
    }

    void Update()
    {
        Vector2 offset = new Vector2(Time.time * Speed, 0);
        renderer.sharedMaterial.SetTextureOffset("_MainTex", offset);
    }
}