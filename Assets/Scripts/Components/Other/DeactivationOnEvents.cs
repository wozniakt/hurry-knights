﻿using UnityEngine;
using System.Collections;

public class DeactivationOnEvents : MonoBehaviour
{
    [SerializeField]
    GameObject effectPrefab;
    // Use this for initialization
    void Start()
    {
        GlobalEventsManager.instance.StartListening("BossAppears", DisableOnBossActivation);
    }

    // Update is called once per frame
    void DisableOnBossActivation(Hashtable eventParams)
    {
        GameObject effect = FactorySingletons.Instance.PoolManager.SpawnGoInstance(effectPrefab);
        effect.transform.position = this.transform.position;
        this.gameObject.SetActive(false);
    }
}
