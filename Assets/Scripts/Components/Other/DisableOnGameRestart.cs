﻿using UnityEngine;
using System.Collections;

public class DisableOnGameRestart : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        GlobalEventsManager.instance.StartListening("GameStateChanged", OnGameStateChange);
    }


    void OnGameStateChange(Hashtable eventParams)
    {
        GameStateManager.GameState gameState = (GameStateManager.GameState)eventParams["GameState"];
        if (this.CompareTag("Hero"))
        {
            if (gameState== GameStateManager.GameState.Win)
            {
                return;
            }
            
          
        }
        this.gameObject.SetActive(gameState==GameStateManager.GameState.GameOn);
       
    }
}
