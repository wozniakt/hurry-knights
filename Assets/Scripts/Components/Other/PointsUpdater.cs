﻿using UnityEngine;
using System.Collections;
using TMPro;
using System.Collections.Generic;

public class PointsUpdater : MonoBehaviour
{
    [SerializeField] bool tutorialDialogs=true, initialDialogs=true, otherDialogs=true;
    [SerializeField] GameObject PointsTextGo;
    [SerializeField] TextMeshProUGUI PointsText;
    [SerializeField] float showDialogPointsMultipler = 50;
    float counter = 1;
    float currentPoints;
    bool isHeroRunning;
    float timer;
    // Use this for initialization
    void Start()
    {
        counter = 1;
        GlobalEventsManager.instance.StartListening("GameStateChanged", StartCountingPoints);

    }
    void StartCountingPoints(Hashtable eventParams)
    {

        GameStateManager.GameState gameState = (GameStateManager.GameState)eventParams["GameState"];
        if (gameState == GameStateManager.GameState.GameOn)
        {
            isHeroRunning = true;
            currentPoints = 0;
            timer = 0;
        }
        else
        {
            isHeroRunning = false;
            GlobalEventsManager.instance.TriggerEvent("GivePoints",new Hashtable { { "Points",(int) currentPoints } });
            
        }
       
       
    }
    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        if (FactorySingletons.Instance.GameStateMan.CurrentGameState== GameStateManager.GameState.GameOn && isHeroRunning && FactorySingletons.Instance.GameStateMan.DialogTutorialOn==false)
        {
            if (timer>1)
            {
                FactorySingletons.Instance.PlayerDataManager.CurrentPlayerData.CurrentPoints += 1;
                timer = 0;
            }

            PointsText.text ="COINTS "+ FactorySingletons.Instance.PlayerDataManager.CurrentPlayerData.CurrentPoints.ToString();

            if (tutorialDialogs && FactorySingletons.Instance.PlayerDataManager.CurrentPlayerData.CurrentPoints == 0 && FactorySingletons.Instance.GameStateMan.DialogOn == false && FactorySingletons.Instance.PlayerDataManager.CurrentPlayerData.ShowTutorial)
            {
                FactorySingletons.Instance.GameStateMan.DialogOn = true;
                FactorySingletons.Instance.GameStateMan.DialogTutorialOn = true;
                GlobalEventsManager.instance.TriggerEvent("StartTalking", new Hashtable { { "Dialog", FactorySingletons.Instance.PlayerDataManager.CurrentPlayerData.CurrentHeroesList.tutorialDialogsScriptObj.GetRandomDialog() } }); //todo: get random dialog
                FactorySingletons.Instance.PlayerDataManager.CurrentPlayerData.ShowTutorial = false;
            }
            else if (initialDialogs && FactorySingletons.Instance.PlayerDataManager.CurrentPlayerData.CurrentPoints == 0 && FactorySingletons.Instance.GameStateMan.DialogOn == false)
            {
                FactorySingletons.Instance.GameStateMan.DialogOn = true;
                FactorySingletons.Instance.GameStateMan.DialogInitialOn = true;
                GlobalEventsManager.instance.TriggerEvent("StartTalking", new Hashtable { { "Dialog", FactorySingletons.Instance.PlayerDataManager.CurrentPlayerData.CurrentHeroesList.initialDialogsScriptObj.GetRandomDialog() } }); //todo: get random dialog
               

            }

            //if (otherDialogs && FactorySingletons.Instance.PlayerDataManager.CurrentPlayerData.CurrentPoints > (showDialogPointsMultipler * counter) && FactorySingletons.Instance.GameStateMan.DialogOn == false)
            //{
            //    FactorySingletons.Instance.GameStateMan.DialogOn = true;
            //    GlobalEventsManager.instance.TriggerEvent("StartTalking", new Hashtable { { "Dialog", FactorySingletons.Instance.PlayerDataManager.CurrentPlayerData.CurrentHeroesList.dialogsScriptObj.GetRandomDialog() } }); //todo: get random dialog
            //    counter++;
            //}

                if (otherDialogs && FactorySingletons.Instance.GameStateMan.StartSpecialDialog  && FactorySingletons.Instance.GameStateMan.DialogOn == false)
                {
                    FactorySingletons.Instance.GameStateMan.DialogOn = true;
                    GlobalEventsManager.instance.TriggerEvent("StartTalking", new Hashtable { { "Dialog", FactorySingletons.Instance.PlayerDataManager.CurrentPlayerData.CurrentHeroesList.dialogsScriptObj.GetRandomDialog() } }); //todo: get random dialog
                    counter++;
                }
  
            
        }
        
    }
}
