﻿

using UnityEngine;
using System.Collections;

public class DisableAfterTime : MonoBehaviour
{
    [SerializeField] float timeToDisable = 4;
    float timer = 0;



    // Use this for initialization
    void OnEnable()
    {
        timer = 0;
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if (timer > timeToDisable)
        {
            timer = 0;

            this.gameObject.SetActive(false);
        }

    }
}
