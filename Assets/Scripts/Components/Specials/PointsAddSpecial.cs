﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Collections;

public class PointsAddSpecial : MonoBehaviour, ISpecialActivate
{
    [SerializeField] int points;

    public void ActivateSpecial(GameObject heroGO)
    {
        FactorySingletons.Instance.GameStateMan.SpecialActive = true;
        GlobalEventsManager.instance.TriggerEvent("GivePoints", new Hashtable { { "Points", points } });
        FactorySingletons.Instance.GameStateMan.SpecialActive = false;
        GetComponent<IDeath>().Die();
        GlobalEventsManager.instance.TriggerEvent("SpecialDied");
    }

}
