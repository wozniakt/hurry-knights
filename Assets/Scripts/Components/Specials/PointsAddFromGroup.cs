﻿using UnityEngine;
using System.Collections;

public class PointsAddFromGroup : MonoBehaviour
{

    [SerializeField] int points;
    // Use this for initialization
    private void OnTriggerEnter2D(Collider2D col)
    {

        if (col.gameObject.CompareTag("Hero"))
        {
            GlobalEventsManager.instance.TriggerEvent("GivePoints", new Hashtable { { "Points", points } });
            GetComponent<IDeath>().Die();
        }
    }
}
