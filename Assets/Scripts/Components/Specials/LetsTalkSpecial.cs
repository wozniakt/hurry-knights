﻿using UnityEngine;
using System.Collections;

public class LetsTalkSpecial : MonoBehaviour, ISpecialActivate
{
    

    public void ActivateSpecial(GameObject heroGO)
    {
        FactorySingletons.Instance.GameStateMan.StartSpecialDialog = true;
        GetComponent<IDeath>().Die();
        GlobalEventsManager.instance.TriggerEvent("SpecialDied");
    }

}

