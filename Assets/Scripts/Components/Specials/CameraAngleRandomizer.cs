﻿using UnityEngine;
using System.Collections;

public class CameraAngleRandomizer : MonoBehaviour
{
    [SerializeField] float angleBoundary = 10;
    [SerializeField] float targetAngleBoundary ;
    [SerializeField] float rndDelayTimeMax, rndDelayTimeMin;
    [SerializeField] float rndDelayTime=50;
    public float TimerSpecial = 5f;
    [SerializeField] Camera rotationCamera;
    float timerUpsideDown;
    float z;
    public bool UpsideDown;
    float timer;
    bool forward = true;
    [SerializeField] float zMultiplerCurrent = 0.1f;
    [SerializeField] float zMultiplerBase = 0.1f;
    [SerializeField] float zMultiplerIncrease = 0.01f;
    [SerializeField] float maxZmultipler = 1f;
    bool bossAppear;

    private void Start()
    {
        GlobalEventsManager.instance.StartListening("BossAppears", OnBossAppear);

        rotationCamera.transform.eulerAngles = new Vector3(0, 0, 0);
 
        targetAngleBoundary = angleBoundary;
        // RandomizeAndReset();
    }

    void OnBossAppear(Hashtable eventParams)
    {

        bossAppear = true;
        targetAngleBoundary = 0;
        zMultiplerCurrent = 0.1f;
    }
    private void OnDisable()
    {
        bossAppear = false;
        zMultiplerCurrent = zMultiplerBase;
    }
    // Use this for initialization
    void Update()
    {
        if (FactorySingletons.Instance.GameStateMan.CurrentGameState == GameStateManager.GameState.GameOn)
        {
            timer += Time.deltaTime;

            if (bossAppear==true)
            {
                targetAngleBoundary = 0;
               
            }
 
            if (timer > rndDelayTime && z <= targetAngleBoundary && forward || (bossAppear == true && forward==true  && z <= targetAngleBoundary) )
            {

                z += zMultiplerCurrent;


                if (z >= targetAngleBoundary)
                {
                    forward = false;
                    RandomizeAndReset();

                }
            }
            if (timer > rndDelayTime && z >= -targetAngleBoundary && forward == false || (bossAppear == true && forward==false && z >= -targetAngleBoundary))
            {
      

                z -= zMultiplerCurrent;



                if (z <= -targetAngleBoundary)
                {
                    forward = true;
                    RandomizeAndReset();
                }
            }

            rotationCamera.transform.eulerAngles = new Vector3(0, 0, z);
            return;
        }
    }

    void RandomizeAndReset()
    {
        timer = 0;
        rndDelayTime = Random.Range(rndDelayTimeMin, rndDelayTimeMax);
        int rndAngle = Random.Range(0, 2);
        if (rndAngle == 0 || bossAppear==true)
        {
 
            targetAngleBoundary = 0;
        }
        else
        {
            targetAngleBoundary = angleBoundary;
        }
    }


}
