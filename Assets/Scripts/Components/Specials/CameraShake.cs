﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class CameraShake : MonoBehaviour
{
   [SerializeField] float duration=3;
    [SerializeField] float power = 1;
    bool startShake;
    float timer;
    // Use this for initialization
    void Start()
    {
        GlobalEventsManager.instance.StartListening("ShakeCamera", ShakeCamera);
    }
    void ShakeCamera(Hashtable eventParams)
    {
     
        duration = (float)eventParams["Duration"];

        StartCoroutine(Shake());
    }

    // Update is called once per frame
    IEnumerator Shake()
    {
        Tween myTween = transform.DOShakePosition(duration, power);
        yield return myTween.WaitForCompletion();
    
    }
}
