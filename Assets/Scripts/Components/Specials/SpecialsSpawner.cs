﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpecialsSpawner : MonoBehaviour
{
    public enum Direction { Left, Right };
    [SerializeField] float specialSpeed;
    [SerializeField] Direction startingDirection;
    List<Special> availableSpecials;
    // GameObject enemyPrefab;
    public bool isSpawning = false;
    GameObject specialGO;
     float rndSpawnDelay ;
    [SerializeField] float currentSpawnDelay = 1;
    [SerializeField] float minSpawnDelay = 1;
    [SerializeField]
    float maxSpawnDelay = 3f;
    float timer = 0;
    // Use this for initialization
    void Start()
    {
        GlobalEventsManager.instance.StartListening("GameStateChanged", OnGameStateChange);
        GlobalEventsManager.instance.StartListening("SpecialDied", OnSpecialDied);
    }

    void OnSpecialDied(Hashtable eventParams)
    {
       //if (FactorySingletons.Instance.GameStateMan.CurrentGameState == GameStateManager.GameState.GameOn)
        {
            FactorySingletons.Instance.GameStateMan.SpecialActive = false;
            timer = 0;
            currentSpawnDelay = Random.Range(minSpawnDelay, 3f);
            isSpawning = true;
        };
    }

    void OnGameStateChange(Hashtable eventParams)
    {
        GameStateManager.GameState gameState = (GameStateManager.GameState)eventParams["GameState"];
        availableSpecials = FactorySingletons.Instance.GlobalDataContainer.UnlockedSpecials;
        timer = 0;
        isSpawning = (gameState == GameStateManager.GameState.GameOn);
        FactorySingletons.Instance.GameStateMan.SpecialActive =false;
    }


    public GameObject GetRandomSpecial()
    {
       
        GameObject rndSpecial = availableSpecials[UnityEngine.Random.Range(0, availableSpecials.Count)].prefab;
        //Debug.Log(rndEnemy.tag +"  "+ FactorySingletons.Instance.GameStateMan.SpecialActive);
        //if (FactorySingletons.Instance.GameStateMan.SpecialActive == true && rndEnemy.CompareTag("Special") == true)
        //{
        //    rndEnemy = GetRandomEnemy();
        //}
        return rndSpecial;
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if (timer > currentSpawnDelay && isSpawning && FactorySingletons.Instance.GameStateMan.CurrentGameState == GameStateManager.GameState.GameOn && FactorySingletons.Instance.GameStateMan.DialogOn == false)//&& FactorySingletons.Instance.GameStateMan.CurrentCartsCounter < 1)
        {
            GameObject specialPrefab = GetRandomSpecial();
            specialGO = FactorySingletons.Instance.PoolManager.SpawnGoInstance(specialPrefab);
            // enemyGO = Instantiate(enemyPrefab);
            specialGO.SetActive(true);

            IMove moveComp = specialGO.GetComponent<IMove>();

            moveComp.Speed = specialSpeed;

            specialGO.transform.position = this.transform.position;

            isSpawning = false;
            currentSpawnDelay = Random.Range(minSpawnDelay, maxSpawnDelay);
        }

    }
}

