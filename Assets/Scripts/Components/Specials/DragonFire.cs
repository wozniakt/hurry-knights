﻿using UnityEngine;
using System.Collections;

public class DragonFire : MonoBehaviour
{
    [SerializeField] GameObject attackGo;
    [SerializeField] float attackDelay=0.5f;
    float timer = 0;
    // Use this for initialization
    void OnDisable()
    {
        timer = 0;
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if (timer> attackDelay)
        {
            attackGo.SetActive(true);
        }
    }
}
