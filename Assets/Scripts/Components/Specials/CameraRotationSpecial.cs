﻿using System.Collections;
using UnityEngine;


public class CameraRotationSpecial : MonoBehaviour, ISpecialDeactivate
{
    [SerializeField] float angleBoundary=45;
    [SerializeField] float delayTime = 0.1f;
    public float TimerSpecial = 5f;
    [SerializeField] Camera rotationCamera;
    float timerUpsideDown;
    float z;
    public bool UpsideDown;
    float timer;
    bool forward = true;
    [SerializeField] float zMultipler=0.1f;
    [SerializeField] float zMultiplerIncrease = 0.01f;
    [SerializeField] float maxZmultipler = 1f;
    private void Start()
    {
        GlobalEventsManager.instance.StartListening("GameStateChanged", OnGameStateChange);
        GlobalEventsManager.instance.StartListening("CameraSpecialUpsideDown", OnCameraSpecialUpsideDown);

    }
    void OnGameStateChange(Hashtable eventParams)
    {
        GameStateManager.GameState gameState = (GameStateManager.GameState)eventParams["GameState"];
        if (gameState==GameStateManager.GameState.GameOn || gameState == GameStateManager.GameState.Lost)
        {
            timer = 0;
            timerUpsideDown = 0;
            UpsideDown = false;
            FactorySingletons.Instance.GameStateMan.SpecialActive = false;
            return;
        }

    }

    void OnCameraSpecialUpsideDown(Hashtable eventParams)
    {

        timerUpsideDown = 0;
        timer = 0;
        UpsideDown = true;
    }

    private void Update()
    {

        if (UpsideDown == false)
        {
            if (FactorySingletons.Instance.GameStateMan.CurrentGameState == GameStateManager.GameState.GameOn)
            {
                timer += Time.deltaTime;

                if (timer > delayTime)
                {
                    if (forward)
                    {
                        z += zMultipler;
                    }
                    if (forward == false)
                    {
                        z -= zMultipler;
                    }

                    if (z >= angleBoundary)
                    {
                        forward = false;
                        zMultipler = zMultipler + zMultiplerIncrease;
                    }
                    if (z <= -angleBoundary)
                    {
                        forward = true;
                        zMultipler = zMultipler + zMultiplerIncrease;
                    }
                    if (zMultipler>maxZmultipler)
                    {
                        zMultipler = -0.0f;
                    }
                    if (zMultipler < -maxZmultipler)
                    {
                        zMultipler = 0.0f;
                    }
                    rotationCamera.transform.eulerAngles = new Vector3(0, 0, z);
                    return;
                }
                //rotationCamera.transform.eulerAngles = new Vector3(0, 0, 0);
                //return;
            }
        }

            if (UpsideDown)
            {
                //Debug.Log("timer: " + timer+ "TimerSpecial: " + TimerSpecial);
                timerUpsideDown += Time.deltaTime;
                if (timerUpsideDown > delayTime && timerUpsideDown < TimerSpecial)
                {
           
                    rotationCamera.transform.eulerAngles = new Vector3(0, -360, 0);
            }
                if (timerUpsideDown > delayTime && timerUpsideDown > TimerSpecial)
                {

                rotationCamera.transform.eulerAngles = new Vector3(0, 0, 0);
                UpsideDown = false;
                    forward = true;
                    DeactivateSpecial();
                }
            }
        }
  

    public void DeactivateSpecial()
    {
        FactorySingletons.Instance.GameStateMan.SpecialActive = false;

        GlobalEventsManager.instance.TriggerEvent("SpecialDied");
    }
}
