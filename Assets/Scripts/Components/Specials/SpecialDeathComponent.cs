﻿
using UnityEngine;
using System.Collections;

public class SpecialDeathComponent : MonoBehaviour, IDeath
{
    [SerializeField] GameObject deathEffect;
    [SerializeField] Transform deathEffectPos;
    public void Die()
    {

        //GetComponent<GivePointsComponent>().GivePoints();
        GameObject deathEffectGo = FactorySingletons.Instance.PoolManager.SpawnGoInstance(deathEffect);
        if (deathEffect!=null)
        {
            deathEffectGo.SetActive(true);
            if (deathEffectPos!=null)
            {
                deathEffectGo.transform.position = deathEffectPos.position;
            }
            else
            {
                deathEffectGo.transform.position = this.transform.position;
            }
           
            FactorySingletons.Instance.GameStateMan.SpecialActive = false;
        }

        GlobalEventsManager.instance.TriggerEvent("SpecialDied");
        GetComponent<IDisable>().DisableGO();
    }



}