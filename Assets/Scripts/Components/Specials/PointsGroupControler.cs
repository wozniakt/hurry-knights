﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PointsGroupControler : MonoBehaviour
{
    [SerializeField] List<GameObject> pointsGameObjects;
    // Use this for initialization
    int rnd;
    void OnEnable()
    {
        foreach (GameObject item in pointsGameObjects)
        {
            rnd = Random.Range(0, 2);
            item.SetActive(rnd==0);
        }
    }


}
