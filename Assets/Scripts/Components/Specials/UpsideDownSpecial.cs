﻿    
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;



public class UpsideDownSpecial : MonoBehaviour, ISpecialActivate
{

    [SerializeField] GameObject cameraMain;


    public void ActivateSpecial(GameObject heroGO)
    {
        FactorySingletons.Instance.GameStateMan.SpecialActive = true;
        GlobalEventsManager.instance.TriggerEvent("CameraSpecialUpsideDown");
       // cameraMain.GetComponent<CameraRotationSpecial>().UpsideDown = true;
    }

}
