﻿using UnityEngine;
using System.Collections;

public class DisableSpecialAfterTime : MonoBehaviour, ISpecialDeactivate
{
    [SerializeField] GameObject deathEffect;
    [SerializeField] float timeToDisable=5;
    float timer=0;

    public void DeactivateSpecial()
    {
        FactorySingletons.Instance.GameStateMan.SpecialActive = false;
        if (deathEffect!=null)
        {
            GameObject deathEffectGo = FactorySingletons.Instance.PoolManager.SpawnGoInstance(deathEffect);
            deathEffectGo.SetActive(true);
            deathEffectGo.transform.position = this.transform.position;
        }

        GlobalEventsManager.instance.TriggerEvent("SpecialDied");
    }

    // Use this for initialization
    void OnEnable()
    {
        timer = 0;
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if (timer>timeToDisable)
        {
            timer = 0;
            DeactivateSpecial();
            this.gameObject.SetActive(false);
        }

    }
}
