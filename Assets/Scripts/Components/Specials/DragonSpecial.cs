﻿using UnityEngine;
using System.Collections;


using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;



public class DragonSpecial : MonoBehaviour, ISpecialActivate
{
    
    [SerializeField] Transform dragonPos;
    [SerializeField]    GameObject appearEffect;

    public void ActivateSpecial(GameObject heroGO)
    {
        GlobalEventsManager.instance.TriggerEvent("ShakeCamera", new Hashtable { {"Duration", 2f } });
        FactorySingletons.Instance.GameStateMan.SpecialActive = true;
        GameObject dragon = FactorySingletons.Instance.PoolManager.SpawnGoInstance(FactorySingletons.Instance.PlayerDataManager.CurrentPlayerData.CurrentHero.SpecialDragon);
        dragon.transform.position = dragonPos.position;
        dragon.SetActive(true);
        GameObject appearEffectGo = FactorySingletons.Instance.PoolManager.SpawnGoInstance(appearEffect);
        appearEffectGo.SetActive(true);
        appearEffectGo.transform.position = dragon.transform.position;
    }


}
