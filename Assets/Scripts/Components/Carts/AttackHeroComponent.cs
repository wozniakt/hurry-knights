﻿using UnityEngine;
using System.Collections;

public class AttackHeroComponent : MonoBehaviour
{
    [SerializeField] bool slash, jab;
    
    ICharacterController characterController;
    [SerializeField] float animTimerCooldown, attackTimerCooldown=0.2f;
    [SerializeField] bool respectAttackingCooldown;
    [SerializeField] GameObject attackPrefab;
    public bool isAttacking=false;
    public bool IsHoldingButton;
    [SerializeField] float attackDelay = 0.1f;
    Vector3 attackPrefabStartPos;
    float timer;
    int rndAttack;
    // Use this for initialization
    void Start()
    {
        GlobalEventsManager.instance.StartListening("HeroAttack", AttackHero);
        GlobalEventsManager.instance.StartListening("HeroAttackStop", StopAttack);
        characterController = GetComponent<ICharacterController>();
        attackPrefabStartPos = attackPrefab.transform.localPosition;
    }
    void StopAttack(Hashtable eventParams)
    {
        IsHoldingButton = false;
    }


    // Update is called once per frame
    void AttackHero(Hashtable eventParams)
    {
        attackPrefab.transform.localPosition = attackPrefabStartPos;
        IsHoldingButton = true;
       // if (isAttacking == false)
        {
            if (slash && jab == false)
            {
                characterController.ChangeAnimationTrigger("Slash");
            }
            else if (slash == false && jab)

            {
                characterController.ChangeAnimationTrigger("Jab");
            }
            else if ((slash && jab) || (slash == false && jab == false))
            {
                RandomAttack();
            }

            //attackPrefab.transform.localPosition = attackPrefabStartPos;
            isAttacking = true;
            timer = 0;
        }



        //if (timer < attackTimerCooldown && timer != 0)
        //{
        //    return;
        //}

        //if (respectAttackingCooldown == true)
        //{
        //    if (isAttacking == false)
        //    {
        //        if (slash && jab==false)
        //        {
        //            characterController.ChangeAnimationTrigger("Slash");
        //        }
        //        else if (slash==false && jab)

        //        {
        //            characterController.ChangeAnimationTrigger("Jab");
        //        }
        //        else if ((slash  && jab) || (slash==false && jab==false))
        //        {
        //            RandomAttack();
        //        }      

        //        attackPrefab.transform.localPosition = attackPrefabStartPos;
        //        isAttacking = true;
        //        timer = 0;
        //    }
        //}

        //else if(  respectAttackingCooldown == false)
        //{
        //    if (slash && jab == false)
        //    {
        //        characterController.ChangeAnimationTrigger("Slash");
        //    }
        //    else if (slash == false && jab)

        //    {

        //        characterController.ChangeAnimationTrigger("Jab");
        //    }
        //    else if ((slash && jab) || (slash == false && jab == false))
        //    {
        //        RandomAttack();
        //    }
        //    attackPrefab.transform.localPosition = attackPrefabStartPos;
        //    isAttacking = true;
        //    timer = 0;
        //}

        // todo: appear prefabAttack with 0.5 sec time to disable
    }
    void RandomAttack()
    {
        rndAttack = Random.Range(0, 2);
        if (characterController != null && rndAttack == 0)
        {
            characterController.ChangeAnimationTrigger("Slash");
        }
        if (characterController != null && rndAttack == 1)
        {
            characterController.ChangeAnimationTrigger("Jab");
        }
    }

    private void Update()
    {
        if (isAttacking)
        {
            attackPrefab.SetActive(true);
            //timer += Time.deltaTime;
            //if (timer >= attackDelay)
            //{
            //    attackPrefab.SetActive(true);
            //}

            //if (timer>= attackTimerCooldown)
            //{
            //    attackPrefab.SetActive(false);
            //    isAttacking = false;
            //    timer = 0;
            //}
        }
        else
        {
            if (IsHoldingButton)
            {
                AttackHero(new Hashtable { {"","" } });
                return;
            }
            attackPrefab.SetActive(false);
        }

    }
}
