﻿using UnityEngine;
using System.Collections;

public class MoveStraightCart : MonoBehaviour, IMove
{
    [SerializeField] float speed;
    public float Speed { get { return speed; } set { speed = value; } }

    public bool IsMoving { get; set; }
    public Vector3 Direction { get; set; }
    [SerializeField] SpriteRenderer spriteRenderer;

    void Start()
    {
       
    }
    public void ChangeDirection(CartsSpawner.Direction direction)
    {
   
        if (direction== CartsSpawner.Direction.Left)
        {
            Direction = Vector2.left;
            transform.eulerAngles = new Vector3(0, -180, 0);
           // spriteRenderer.flipX = true;
        }
        else
        {
            Direction = Vector2.right;
            transform.eulerAngles = new Vector3(0, 0, 0);
            //spriteRenderer.flipX = false;
        }
        
    }
    public void ChangeDirectionToOpposite()
    {
        Direction = -Direction;
        if (Direction.x ==-1)
        {
            //spriteRenderer.flipX = true;
            transform.eulerAngles = new Vector3(0, -180, 0);
            //ChangeSpeed(-Speed);
        }
        else
        {
            transform.eulerAngles = new Vector3(0, 0, 0);
          //  spriteRenderer.flipX = false;
        }
    }
    public void ChangeMoveState(bool isMoving)
    {
        IsMoving = isMoving;
    }

    public void ChangeRotation(Vector2 rotationDir)
    {
        transform.localEulerAngles = rotationDir;
    }


    public void ChangeSpeed(float newSpeed)
    {
        Speed = newSpeed;
    }

    public float GetCurrentSpeed()
    {
        return Speed;
    }


    // Update is called once per frame
    void FixedUpdate()
    {
        if (IsMoving)
        {
            transform.Translate(Direction.x* speed * Time.deltaTime, 0, 0);
        }

    }
}