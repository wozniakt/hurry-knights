﻿using UnityEngine;
using System.Collections;

public class HeroDeathComponent : MonoBehaviour, IDeath
{
    [SerializeField] GameObject deathEffect;
    public void Die()
    {

        GameObject deathEffectGo = FactorySingletons.Instance.PoolManager.SpawnGoInstance(deathEffect);
        deathEffectGo.SetActive(true);
        deathEffectGo.transform.position = this.transform.position;
        GetComponent<IDisable>().DisableGO();
        FactorySingletons.Instance.GameStateMan.ChangeGameState(GameStateManager.GameState.Lost);
    }

}
