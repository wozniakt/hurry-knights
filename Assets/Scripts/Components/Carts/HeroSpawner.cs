﻿using UnityEngine;
using System.Collections;

public class HeroSpawner : MonoBehaviour
{
 GameObject heroPrefab;
    GameObject heroGO;
    [SerializeField] bool sidekick;
    void Start()
    {
        GlobalEventsManager.instance.StartListening("GameStateChanged", OnGameStateChange);
    }


    void OnGameStateChange(Hashtable eventParams)
    {
        GameStateManager.GameState gameState = (GameStateManager.GameState)eventParams["GameState"];

        if (gameState == GameStateManager.GameState.GameOn)
        {
            
            if (sidekick == true)
            {
                heroPrefab = FactorySingletons.Instance.PlayerDataManager.CurrentPlayerData.CurrentHero.SidekickPrefab;
            }
            else
            {
                heroPrefab = FactorySingletons.Instance.PlayerDataManager.CurrentPlayerData.CurrentHero.Prefab;
            }
                heroGO = FactorySingletons.Instance.PoolManager.SpawnGoInstance(heroPrefab);
            heroGO.GetComponent<HeroParticles>().SwitchStateParticles(true);

            //heroGO = Instantiate(heroPrefab);
            heroGO.SetActive(true);
            heroGO.transform.position = this.transform.position;

        }
    }


}
