﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HeroParticles : MonoBehaviour
{
    [SerializeField] List<GameObject >Particles;
    // Use this for initialization
    public void SwitchStateParticles(bool isOn)
    {
        foreach (GameObject particle in Particles)
        {
            particle.SetActive(isOn);
        }
    }


}
