﻿using UnityEngine;
using System.Collections;

public class CartsSpawner : MonoBehaviour
{
    public enum Direction { Left, Right};
    [SerializeField] float cartSpeed;
    [SerializeField] Direction startingDirection;
    [SerializeField] GameObject cartPrefab;
    public bool isSpawning = false;
    GameObject cartGO;
    // Use this for initialization
    //void Start()
    //{
    //    GlobalEventsManager.instance.StartListening("GameStateChanged", OnGameStateChange);
    //}


    //void OnGameStateChange(Hashtable eventParams)
    //{
    //    GameStateManager.GameState gameState = (GameStateManager.GameState)eventParams["GameState"];

    //    isSpawning = (gameState == GameStateManager.GameState.GameOn);
    //}
    // Update is called once per frame
    void Update()
    {
        if (isSpawning && FactorySingletons.Instance.GameStateMan.CurrentGameState==GameStateManager.GameState.GameOn &&  FactorySingletons.Instance.GameStateMan.CurrentCartsCounter < 1)
        {
            cartPrefab = FactorySingletons.Instance.PlayerDataManager.CurrentPlayerData.CurrentHero.Prefab;
            //cartGO = FactorySingletons.Instance.PoolManager.SpawnGoInstance(cartPrefab);
            cartGO = Instantiate(cartPrefab);
            cartGO.SetActive(true);
            //IMove moveComp = cartGO.GetComponent<IMove>();
            //moveComp.ChangeDirection(startingDirection);
            //moveComp.ChangeMoveState(true);
            
            //if (startingDirection== Direction.Left && moveComp.Speed>0)
            //{
            //    moveComp.Speed = -moveComp.Speed;
            //}
            //if (startingDirection == Direction.Right && moveComp.Speed < 0)
            //{
            //    moveComp.Speed = -moveComp.Speed;
            //}
            cartGO.transform.position = this.transform.position;
            isSpawning = false;
        }

    }
}
