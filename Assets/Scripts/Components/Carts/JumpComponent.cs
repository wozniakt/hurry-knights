﻿using UnityEngine;
using System.Collections;

public class JumpComponent : MonoBehaviour
{

    [SerializeField] GameObject smokeParticleGo;
    [SerializeField] Rigidbody2D rigidBody2d;
    [SerializeField] Vector2 force;
    [SerializeField]bool isJumping=false;
    ICharacterController characterController;

    public bool IsJumping
    {
        get
        {
            return isJumping;
        }
        set
        {
            smokeParticleGo.SetActive(!value);
            isJumping = value;
        }
    }

    void Start()
    {
        GlobalEventsManager.instance.StartListening("HeroJump", Jump);
         characterController = GetComponent<ICharacterController>();
    }
    // Use this for initialization
    void Jump(Hashtable eventParams)
    {
        
        if (!IsJumping)
        {
   
            rigidBody2d.AddForce(force);
            IsJumping = true;
    

        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.gameObject.CompareTag("Floor")==true)
        {
            IsJumping = false;
            if (characterController != null)
            {
                characterController.ChangeAnimationBool("Stand", IsJumping);
                characterController.ChangeAnimationBool("Run", !IsJumping);
            }
        }

    }


}
