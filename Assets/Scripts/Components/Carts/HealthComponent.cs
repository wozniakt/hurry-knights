﻿using UnityEngine;
using System.Collections;

public class HealthComponent : MonoBehaviour, IHealth
{
    public int CurrentHealth, MaxHealth;
    // Use this for initialization
    void Start()
    {
        CurrentHealth = MaxHealth;
    }

    // Update is called once per frame
    public void DecreaseHealth(int changeAmount)
    {
        CurrentHealth -= changeAmount;
        if (CurrentHealth<=0)
        {
            GetComponent<IDeath>().Die();
        }
    }
}
