﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[Serializable]
public class Special
{
    public GameObject prefab;
    public Sprite spriteImg;
    public string Name;
    public string Description;
    public int unlockValue;
    public bool Unlocked;
    //bool unlocked;
    //public bool Unlocked
    //{
    //    get
    //    {

    //        return unlocked;
    //    }
    //    set
    //    {
    //        unlocked = value;
    //    }
    //}
}

