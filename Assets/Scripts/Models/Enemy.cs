﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[Serializable]
public class Enemy
{
    public GameObject prefab;
    public Sprite spriteImg;
    public string Name;

    public int unlockValue;
    public bool Unlocked { get; set; }
}
