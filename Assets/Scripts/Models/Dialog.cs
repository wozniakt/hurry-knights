﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

[Serializable]
public class Dialog 
{
    public List<string> DialogTexts;
    public bool alreadyUsed;
    public float ShowTime;
    //public GameObject talkingCharacter;
    //public GameObject bubblePrefab;
}
