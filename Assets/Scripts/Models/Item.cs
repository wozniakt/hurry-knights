﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[Serializable]
public class Item
{
    public GameObject prefab;
    public Sprite spriteImg;
    public string Name;
    public string Description;
    public int unlockValue;
    bool unlocked;
    public bool Unlocked
    {
        get
        {

            return unlocked;
        }
        set
        {
            unlocked = value;
        }
    }
}

