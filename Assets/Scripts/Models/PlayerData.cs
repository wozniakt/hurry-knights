﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

[Serializable]
public class PlayerData
{
    [SerializeField] int currentPoints;
    [SerializeField] int hiscorePoints;
    [SerializeField] int currentHp;
    [SerializeField] int maxHp;
    [SerializeField] int currentSponsorIndex;
    [SerializeField] int currentHeroIndex;
    [SerializeField] int curentBgIndex;
    [SerializeField] int curentlevel;

    public bool ShowTutorial=true;
    public int CurrentPoints { get { return currentPoints; } set { currentPoints = value; } }
    public int HiscorePoints { get { return hiscorePoints; } set { hiscorePoints = value; } }
    public int CurrentHp { get { return currentHp; } set { currentHp = value; } }
    public int MaxHp { get { return maxHp; } set { maxHp = value; } }
    public int CurrentSponsorIndex { get { return currentSponsorIndex; } set { currentSponsorIndex = value; } }
    public int CurrentHeroIndex { get { return currentHeroIndex; } set { currentHeroIndex = value; } }
    public int CurrentBgIndex { get { return curentBgIndex; } set { curentBgIndex = value; } }
    public int CurrentLevel { get { return curentlevel; } set { curentlevel = value; } }
    public List<BackgroundElements> BackgroundsElements;
    [SerializeField] MeshRenderer bgPlaceholderSpriteRend;
    //private Background currentBg;
    //public Background CurrentBackground
    //{
    //    get { return currentBg; }
    //    set
    //    {
    //        currentBg = value;
    //        bgPlaceholderSpriteRend.sprite = value.spriteImg;
    //        Resize(bgPlaceholderSpriteRend);
    //    }
    //}

    private Texture currentBg;
    public Texture CurrentBackground
    {
        get { return currentBg; }
        set
        {
            currentBg = value;
           // bgPlaceholderSpriteRend.material.mainTexture = value;
            //Resize(bgPlaceholderSpriteRend);
        }
    }
    [SerializeField] SponsorsListData currentSponsorListData;
    public SponsorsListData CurrentSponsorListData
    {
        get
        {
            return currentSponsorListData;
        }
        set
        {
            currentSponsorListData = value;
          
        }
    } //to


    [SerializeField] Hero currentHero;
    public Hero CurrentHero { get
        { return currentHero; }
        set {
            currentHero = value;
            if (currentHero.BackgroundElements.backgroundTextures[0] == null)
            {
                return;
            }
           // CurrentBackground = value.BackgroundElements.backgroundTextures[0];
            //GlobalEventsManager.instance.TriggerEvent("UpdateBackground",new Hashtable { { "BackgroundElements", GetRandomBackground() } });
        }
    } //todo backgrounds

    BackgroundElements GetRandomBackground()
    {
        int rnd = UnityEngine.Random.Range(0, BackgroundsElements.Count);
        return BackgroundsElements[rnd];
    }

    [SerializeField] SponsorsListData currentHeroesList;
    public SponsorsListData CurrentHeroesList
    {
        get
        { return currentHeroesList; }
        set
        {
            currentHeroesList = value;

            GlobalEventsManager.instance.TriggerEvent("UpdateCurrentHeroesListData", new Hashtable { { "CurrentHeroesListData", value} });
        }
    } //todo backgrounds

    void Resize(SpriteRenderer sr)
    {
        //SpriteRenderer sr = GetComponent<SpriteRenderer>();
        if (sr == null) return;

        sr.transform.localScale = new Vector3(1, 1, 1);

        float width = sr.sprite.bounds.size.x;
        float height = sr.sprite.bounds.size.y;


        float worldScreenHeight = Camera.main.orthographicSize * 2f;
        float worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;

        Vector3 xWidth = sr.transform.localScale;
        xWidth.x = worldScreenWidth / width;
        sr.transform.localScale = xWidth;
        //transform.localScale.x = worldScreenWidth / width;
        Vector3 yHeight = sr.transform.localScale;
        yHeight.y = worldScreenHeight / height;
        sr.transform.localScale = yHeight;
        //transform.localScale.y = worldScreenHeight / height;

    }
}
