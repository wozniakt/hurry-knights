﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[Serializable]
public class Background
{
    public Sprite spriteImg;
    public string Name;
    public int unlockValue;
    bool unlocked;
    public bool Unlocked
    {
        get
        {

            return unlocked;
        }
        set
        {
            unlocked = value;
        }
    }
}
