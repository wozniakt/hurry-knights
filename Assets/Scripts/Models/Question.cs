﻿using UnityEngine;
using System.Collections;

public class Riddle : MonoBehaviour
{
    public string Question;
    public string AnswerGood;
    public string AnswerWrong;
    public bool Used;

}
