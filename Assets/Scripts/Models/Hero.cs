﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[Serializable]
public class Hero
{
    public GameObject Prefab;
    public GameObject SidekickPrefab;
    public Sprite SpriteImg;
    public Sprite SpriteImgSidekick;
    public  Sprite SpriteSmallImg;
    [SerializeField]
    public string Name;
    public string Description;
    public Special Special;
    public int UnlockValue;
    public GameObject SpecialDragon;
    public EnemiesListData EnemiesCollection;
    public BackgroundElements BackgroundElements;

    public bool Unlocked { get; set; }
}
