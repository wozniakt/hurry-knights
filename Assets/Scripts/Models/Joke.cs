﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class Joke 
{

    public string setup, punchline;
    public int id;
}
