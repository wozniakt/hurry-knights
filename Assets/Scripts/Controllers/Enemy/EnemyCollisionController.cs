﻿using UnityEngine;
using System.Collections;

public class EnemyCollisionController : MonoBehaviour
{

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("HeroAttack") && !this.gameObject.CompareTag("Trap"))
        {
 
            //GetComponent<IDeath>().Die();
            GetComponent<IHealth>().DecreaseHealth(1);
        }
        if (collision.gameObject.CompareTag("Hero"))
        {
            collision.gameObject.GetComponent<IHealth>().DecreaseHealth(1);
        }
        if (collision.gameObject.CompareTag("EnemyDespawner"))
        {
            GetComponent<IDisable>().DisableGO();
            GlobalEventsManager.instance.TriggerEvent("EnemyDied");
        }
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.CompareTag("HeroAttack") && !this.gameObject.CompareTag("Trap"))
        {

            //GetComponent<IDeath>().Die();
            GetComponent<IHealth>().DecreaseHealth(1);
        }
    }

    }
