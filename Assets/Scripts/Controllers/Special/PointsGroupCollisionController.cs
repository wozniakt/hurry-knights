﻿using UnityEngine;
using System.Collections;

public class PointsGroupCollisionController : MonoBehaviour
{
    [SerializeField] bool isTreasure;

    // Use this for initialization
    private void OnTriggerEnter2D(Collider2D col)
    {
       
        if (col.gameObject.CompareTag("EnemyDespawner"))
        {
            GetComponent<IDisable>().DisableGO();
            GlobalEventsManager.instance.TriggerEvent("SpecialDied");
            FactorySingletons.Instance.GameStateMan.SpecialActive = false;
        }
    }

}
