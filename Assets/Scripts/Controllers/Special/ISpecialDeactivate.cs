﻿using UnityEngine;

internal interface ISpecialDeactivate
{

    void DeactivateSpecial();
}