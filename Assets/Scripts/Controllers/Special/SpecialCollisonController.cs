﻿using UnityEngine;
using System.Collections;

public class SpecialCollisonController : MonoBehaviour
{
    [SerializeField] bool isTreasure;

    // Use this for initialization
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Hero") && !isTreasure)
        {
            this.GetComponent<ISpecialActivate>().ActivateSpecial(col.gameObject);
            GetComponent<IDisable>().DisableGO();
            GlobalEventsManager.instance.TriggerEvent("SpecialActivated", new Hashtable {{ "TextToShow", this.name }});
        }
        if (col.gameObject.CompareTag("HeroAttack") && isTreasure)
        {
            this.GetComponent<ISpecialActivate>().ActivateSpecial(col.gameObject);
            GetComponent<IDisable>().DisableGO();
            GlobalEventsManager.instance.TriggerEvent("SpecialActivated", new Hashtable { { "TextToShow", this.name } });
        }
        if (col.gameObject.CompareTag("EnemyDespawner"))
        {
            GetComponent<IDisable>().DisableGO();
            GlobalEventsManager.instance.TriggerEvent("SpecialDied");
        }
    }

}
