﻿using UnityEngine;

internal interface ISpecialActivate
{
    void ActivateSpecial(GameObject heroGO);

}