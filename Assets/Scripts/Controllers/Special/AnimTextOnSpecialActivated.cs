﻿using UnityEngine;
using System.Collections;

public class AnimTextOnSpecialActivated : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {
        GlobalEventsManager.instance.StartListening("SpecialActivated", OnSpecialActivated);
    }

    // Update is called once per frame
    void OnSpecialActivated(Hashtable eventParams)
    {

            GetComponent<ITweenable>().DoTweenAnim();

    }

}