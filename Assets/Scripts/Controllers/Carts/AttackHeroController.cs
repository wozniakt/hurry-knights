﻿using UnityEngine;
using System.Collections;

public class AttackHeroController : MonoBehaviour
{

    // Use this for initialization
    public void AttackHero()
    {
        GlobalEventsManager.instance.TriggerEvent("HeroAttack");
    }
    public void StopAttackHero()
    {
        GlobalEventsManager.instance.TriggerEvent("HeroAttackStop");
    }
}
