﻿using UnityEngine;
using System.Collections;

public class HeroCollisionsController : MonoBehaviour
{

    void OnCollisionEnter2D(Collision2D col)
    {
        //Debug.Log("Col cart with: " + col.gameObject.tag);
        //if (col.gameObject.CompareTag("Cart"))
        //{
        //    col.gameObject.GetComponent<IMove>().ChangeDirectionToOpposite();
        //}
        if (col.gameObject.CompareTag("Special"))
        {
            col.gameObject.GetComponent<IDisable>().DisableGO();
            GlobalEventsManager.instance.TriggerEvent("EnemyDied");
        }
        
    }


}
