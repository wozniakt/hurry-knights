﻿using UnityEngine;
using System.Collections;
using Assets.HeroEditor.Common.CharacterScripts;

public class AnimatorCharacterController : MonoBehaviour, ICharacterController
{
    [SerializeField] Animator animator;
    [SerializeField] Character character;
    void OnEnable()
    {
        if (animator!=null)
        {
            animator.SetFloat("Rnd", Random.Range(0.9f, 1.5f));
        }
       
    }


    void Start()
    {//todo: interface animator
        if (character==null)
        {
            return;
        }
        animator = character.Animator;
    }
    public void ChangeAnimationBool(string boolName, bool state)
    {
        if (character == null)
        {
            return;
        }
        animator.SetBool(boolName, state);
    }

    public void ChangeAnimationTrigger(string triggerName)
    {
        if (character == null)
        {
            return;
        }
        animator.SetTrigger(triggerName);
    }

    // Use this for initialization



}
