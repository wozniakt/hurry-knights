﻿using UnityEngine;
using System.Collections;

public class ChangeCartsCounter : MonoBehaviour
{

    // Use this for initialization
    void OnEnable()
    {
        GlobalEventsManager.instance.TriggerEvent("CountersChanged", new Hashtable { {"CartsCounter", +1 } });
    }

    // Update is called once per frame
    void OnDisable()
    {
        GlobalEventsManager.instance.TriggerEvent("CountersChanged", new Hashtable { { "CartsCounter", -1 } });
    }
}
