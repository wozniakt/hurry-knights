﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class AnimTweenOnGameStateController : MonoBehaviour
{
    [SerializeField] GameStateManager.GameState targetGameState;
    // Use this for initialization
    void Start()
    {
        GlobalEventsManager.instance.StartListening("GameStateChanged", OnGameStateChangeWin);
    }

    // Update is called once per frame
    void OnGameStateChangeWin(Hashtable eventParams)
    {
        GameStateManager.GameState gameState = (GameStateManager.GameState)eventParams["GameState"];
        if (gameState == targetGameState)
        {
            GetComponent<ITweenable>().DoTweenAnim();
        }
    }

}