﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CartsSpawnerController : MonoBehaviour
{
    [SerializeField] List< CartsSpawner> cartsSpawners;
    // Use this for initialization
    void Start()
    {
        GlobalEventsManager.instance.StartListening("ChangeSpawningStateCarts", OnChangeCartsCounter);
        GlobalEventsManager.instance.StartListening("GameStateChanged", OnGameStateChange);
    }

    void OnChangeCartsCounter(Hashtable eventParams)
    {
        int rndSpawner = Random.Range(0, cartsSpawners.Count);
        cartsSpawners[rndSpawner].isSpawning = (bool)eventParams["CartsSpawning"];
    }

    void OnGameStateChange(Hashtable eventParams)
    {
        GameStateManager.GameState gameState = (GameStateManager.GameState)eventParams["GameState"];
        if (gameState== GameStateManager.GameState.GameOn)
        {
            int rndSpawner = Random.Range(0, cartsSpawners.Count);
            cartsSpawners[rndSpawner].isSpawning = (gameState == GameStateManager.GameState.GameOn);
        }
     

    }
}
