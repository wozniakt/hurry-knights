﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

public class ItemsSpawnerController : MonoBehaviour
{
    [SerializeField] List<ItemsSpawner> ItemsSpawners;
    int maxActiveSpawners;
    // Use this for initialization

    private void Start()
    {
        GlobalEventsManager.instance.StartListening("GameStateChanged", OnGameStateChange);
        ItemsSpawner[] itemsSpawnerArr = GetComponentsInChildren<ItemsSpawner>();
        ItemsSpawners = new List<ItemsSpawner>(itemsSpawnerArr);
      
    }

    void OnGameStateChange(Hashtable eventParams)
    {
        GameStateManager.GameState gameState = (GameStateManager.GameState)eventParams["GameState"];
        if (gameState== GameStateManager.GameState.GameOn)
        {
            RandomizeItemsSpawners();
        }
       
    }


    void RandomizeItemsSpawners()
    {
        maxActiveSpawners =4+ FactorySingletons.Instance.PlayerDataManager.CurrentPlayerData.HiscorePoints / FactorySingletons.Instance.PlayerDataManager.PointsDivider;

        System.Random rnd = new System.Random();
        var randomizedList = from item in ItemsSpawners
                             orderby rnd.Next()
                             select item;
        List<ItemsSpawner> randomizedItemsSpawners = randomizedList.ToList();
        for (int i = 0; i < randomizedItemsSpawners.Count; i++)
        {
            if (i<maxActiveSpawners)
            {
                randomizedItemsSpawners[i].active = true;
            }
            else
            {
                randomizedItemsSpawners[i].active = false;
            }
            
        }


    }

    // Update is called once per frame
    void Update()
    {

    }
}
