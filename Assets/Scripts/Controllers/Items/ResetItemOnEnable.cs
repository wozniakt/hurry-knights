﻿using UnityEngine;
using System.Collections;

public class ResetItemOnEnable : MonoBehaviour
{
    [SerializeField]
    Rigidbody2D rigidBody2d;
    // Use this for initialization
    void Start()
    {
        if (rigidBody2d == null)
        {
            rigidBody2d = GetComponent<Rigidbody2D>();
        }

    }

    private void OnEnable()
    {
        if (rigidBody2d!=null)
        {
            rigidBody2d.velocity = Vector2.zero;
           
        }

    }


}
