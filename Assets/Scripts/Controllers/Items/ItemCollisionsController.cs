﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(DisableItemComponent))]
public class ItemCollisionsController : MonoBehaviour
{
    [SerializeField] float timer;
    [SerializeField] int damage=1;
    [SerializeField] float collectTime = 2;
    [SerializeField] bool isCollecting=false;
    [SerializeField] int playerHpDamage=1;
    void OnCollisionEnter2D(Collision2D col)
    {
        Debug.Log(col.gameObject.tag);

        if (col.gameObject.CompareTag("Collector") && isCollecting==false)
        {
            timer = 0;
            isCollecting = true;
        }

        if (col.gameObject.CompareTag("Floor") && isCollecting == false &&   FactorySingletons.Instance.GameStateMan.CurrentGameState == GameStateManager.GameState.GameOn)
        {
            GlobalEventsManager.instance.TriggerEvent("PlayerHpChanged", new Hashtable { { "PlayerHp", playerHpDamage } });
            this.gameObject.SetActive(false);
        }
    }
    

    private void FixedUpdate()
    {
        if (isCollecting)
        {
            Debug.Log("isCollecting");
            timer += Time.deltaTime;
            if (timer>= collectTime)
            {
                timer = 0;
                isCollecting = false;
                GetComponent<GivePointsComponent>().GivePoints();
                GetComponent<IDisable>().DisableGO();
                // col.gameObject.GetComponent<HealthComponent>().DecreaseHealth(damage);
            }
        }
   
    }
}
 