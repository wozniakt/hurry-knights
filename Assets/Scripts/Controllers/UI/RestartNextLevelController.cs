﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using System;

public class RestartNextLevelController : MonoBehaviour
{
    private void Start()
    {
        GlobalEventsManager.instance.StartListening("GameStateChanged",GoToNextLevel);
    }

    void GoToNextLevel(Hashtable eventParams)
    {
       
        GameStateManager.GameState gameState = (GameStateManager.GameState)eventParams["GameState"];
        if (gameState != GameStateManager.GameState.Win)
        {
           return;
        }

        RestartToNextLevel();
    }

     void RestartToNextLevel()
    {
        GetComponent<ITweenable>().GetDoTweenAnim((x) =>
        {
            FactorySingletons.Instance.PlayerDataManager.CurrentPlayerData.CurrentLevel++;
            FactorySingletons.Instance.PoolManager.ResetPooledList();
            FactorySingletons.Instance.PlayerDataManager.SavePlayerData();
           //FactorySingletons.Instance.GameStateMan.ChangeGameState(GameStateManager.GameState.GameOn);
           //FactorySingletons.Instance.PlayerDataManager.LoadPlayerData();
           // FactorySingletons.Instance.PlayerDataManager.ResetPlayerHp();
           FactorySingletons.Instance.GameStateMan.LoadGameScene(GameStateManager.GameState.GameOn);
            GlobalEventsManager.instance.TriggerEvent("UpdateBackground");
            FactorySingletons.Instance.PlayerDataManager.LoadPlayerData();

        });
    


    }


}
