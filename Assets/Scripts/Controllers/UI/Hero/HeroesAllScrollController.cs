﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI.Extensions;
using UnityEngine.UI;
using TMPro;
using System;
using Assets.HeroEditor.Common.CharacterScripts;

public class HeroesAllScrollController : MonoBehaviour
{
    //todo: one sponsor has all specials
    // Use this for initialization
    [SerializeField] Button ConfirmButton;
    [SerializeField] Image ConfirmButtonImage;
    [SerializeField] SponsorsListData CurrentHeroesListData;
    [SerializeField] GameObject pagePrefab; //todo: another script to refer to images, etc.
    [SerializeField] HorizontalScrollSnap horizontalScrollSnap;
    bool firstOpenning = true;
    [SerializeField] TextMeshProUGUI textHeroName, textSpecialName;
    [SerializeField] Image previewHero, previewSidekick, previewSpecial;
    GameObject previousPage;
    GameObject heroPreviewGO, sidekickPreviewGO, heroPreviewSmall;
    private void Start()
    {
        GlobalEventsManager.instance.StartListening("ChangePage", ChangePageOnClick);
    }

    void ChangePageOnClick(Hashtable eventParams)
    {

        if (eventParams.ContainsKey("PageIndex"))
        {
            horizontalScrollSnap.ChangePage((int)eventParams["PageIndex"]);
        }
    }

    public void OnShopPanelVisible()
    {
        horizontalScrollSnap.OnSelectionPageChangedEvent.RemoveAllListeners();
       // FactorySingletons.Instance.GlobalDataContainer.PrepareAvailableItems();
       UpdatePages(()=>
        {
            horizontalScrollSnap.OnSelectionPageChangedEvent.AddListener(delegate
            {
                if (horizontalScrollSnap.CurrentPageObject() != null)
                {

                    if (previousPage != null)
                    {
                        ChangePageLook(new Vector2(1, 1), previousPage);
                    }

                    SelectCurrentHero(horizontalScrollSnap.CurrentPageObject().gameObject);

                }
            });
            int currentHeroIndex2 = CurrentHeroesListData.Heroes.IndexOf(FactorySingletons.Instance.PlayerDataManager.CurrentPlayerData.CurrentHero);
            //  horizontalScrollSnap.CurrentPage =currentHeroIndex;
            horizontalScrollSnap.ChangePage(currentHeroIndex2);
            horizontalScrollSnap.UpdateLayout();
            horizontalScrollSnap.UpdateVisible();

        });
        int currentHeroIndex = CurrentHeroesListData.Heroes.IndexOf(FactorySingletons.Instance.PlayerDataManager.CurrentPlayerData.CurrentHero);

        horizontalScrollSnap.ChangePage(currentHeroIndex);
        horizontalScrollSnap.UpdateLayout();
        horizontalScrollSnap.UpdateVisible();
        horizontalScrollSnap.StartingScreen = currentHeroIndex;

    }

 

    // Update is called once per frame
    void UpdatePages(Action callback)
    {
        int counter=0;
        GameObject[] removedChildren;
        horizontalScrollSnap.RemoveAllChildren(out removedChildren);
        foreach (GameObject removedChild in removedChildren)
        {
            removedChild.SetActive(false);
        }
        
        foreach (Hero hero in CurrentHeroesListData.Heroes)
        {
            GameObject newPage = FactorySingletons.Instance.PoolManager.SpawnGoInstance(pagePrefab);
            horizontalScrollSnap.AddChild(newPage);
            newPage.SetActive(true);
            HeroPageComponent heroPage = newPage.GetComponent<HeroPageComponent>();
            hero.Unlocked= (hero.UnlockValue <= FactorySingletons.Instance.PlayerDataManager.CurrentPlayerData.HiscorePoints);
            //if (heroPage.PreviewHeroSmall != null)
            //{
            //    heroPage.PreviewHeroSmall.SetActive(false);
            //}
            if (!hero.Unlocked)
            {
                if (heroPage.PreviewHeroSmall==null)
                {
                    heroPage.PreviewHeroSmall = FactorySingletons.Instance.PoolManager.SpawnGoInstance(hero.Prefab);
                }

                heroPage.PreviewHeroSmall.SetActive(true);
                heroPage.PreviewHeroSmall.GetComponent<Rigidbody2D>().isKinematic = true;

                heroPage.PreviewHeroSmall.transform.SetParent(heroPage.transform);
                heroPage.PreviewHeroSmall.transform.localScale = new Vector3(75f, 75f, 1.0f);
                heroPage.PreviewHeroSmall.transform.localPosition = new Vector3(91, -144, 0);

                // newPage.ImagePage.color = Color.black;
                heroPage.ImageLocked.enabled = true;
                //heroPage.ImageHero.color     = Color.black;
                heroPage.TextMeshLocked.enabled = true;
                heroPage.TextMeshLocked.text = hero.UnlockValue.ToString();
            }
            else
            {
                if (heroPage.PreviewHeroSmall == null)
                {
                    heroPage.PreviewHeroSmall = FactorySingletons.Instance.PoolManager.SpawnGoInstance(hero.Prefab);
                }
                heroPage.PreviewHeroSmall.SetActive(true);
                heroPage.PreviewHeroSmall.GetComponent<Rigidbody2D>().isKinematic = true;

                heroPage.PreviewHeroSmall.transform.SetParent(heroPage.transform);
                heroPage.PreviewHeroSmall.transform.localScale = new Vector3(75, 75, 1.0f);
                heroPage.PreviewHeroSmall.transform.localPosition = new Vector3(91,-144,0);

                //heroPage.ImageHero.color = Color.white;
                heroPage.ImageLocked.enabled = false;
                heroPage.TextMeshLocked.enabled = false;
            }
            heroPage.Index = counter;
           // heroPage.ImageHero.sprite = hero.SpriteSmallImg;
           // heroPage.ImageSidekick.sprite = hero.SpriteImgSidekick;
            heroPage.GetComponent<HeroData>().Data = hero;
            counter++;
        }
        if (counter== CurrentHeroesListData.Heroes.Count)
        {
            callback();

        }
    }


    public void SelectCurrentHero(GameObject currentPage)
    {
        Hero selectedHero = currentPage.GetComponent<HeroData>().Data;

        ChangePageLook( new Vector2(1.1f, 1.1f), currentPage);
        previousPage = currentPage;
        textHeroName.text = selectedHero.Name;
        selectedHero.Unlocked = (selectedHero.UnlockValue <= FactorySingletons.Instance.PlayerDataManager.CurrentPlayerData.HiscorePoints);
        if (selectedHero.Unlocked)
        {
            ConfirmButton.interactable = true;
            ColorBlock theColor = ConfirmButton.GetComponent<Button>().colors;
            theColor.normalColor = Color.white;
            theColor.highlightedColor = Color.white;
            theColor.pressedColor = Color.white;
            ConfirmButton.GetComponent<Button>().colors = theColor;
            ConfirmButtonImage.color = Color.white;
            previewHero.GetComponent<Image>().color = Color.white;
            previewSidekick.GetComponent<Image>().color = Color.white;
            FactorySingletons.Instance.PlayerDataManager.CurrentPlayerData.CurrentHero = selectedHero;
        }
        else
        {
            ConfirmButton.interactable = false;
            ConfirmButtonImage.color = Color.grey;
            ColorBlock theColor = ConfirmButton.GetComponent<Button>().colors;
            theColor.normalColor=Color.gray;
            theColor.highlightedColor = Color.gray;
            theColor.pressedColor = Color.gray;
            ConfirmButton.GetComponent<Button>().colors = theColor;
            previewHero.GetComponent<Image>().color = Color.black;
            previewSidekick.GetComponent<Image>().color = Color.black;
        }
        //previewHero.sprite = selectedHero.SpriteImg;
        if (heroPreviewGO!=null)
        {
            heroPreviewGO.SetActive(false);
        }
        if (sidekickPreviewGO != null)
        {
            sidekickPreviewGO.SetActive(false);
        }

        heroPreviewGO=FactorySingletons.Instance.PoolManager.SpawnGoInstance(selectedHero.Prefab);
        heroPreviewGO.SetActive(true);
        heroPreviewGO.GetComponent<Rigidbody2D>().isKinematic = true;
        heroPreviewGO.transform.localScale = new Vector3(1.6f, 1.6f, 1.6f);
        heroPreviewGO.transform.position = previewHero.transform.position;
        // previewSidekick.sprite= selectedHero.SpriteImgSidekick;
        sidekickPreviewGO = FactorySingletons.Instance.PoolManager.SpawnGoInstance(selectedHero.SidekickPrefab);
        // sidekickPreviewGO = Instantiate(selectedHero.SidekickPrefab);
        sidekickPreviewGO.SetActive(true);
        sidekickPreviewGO.GetComponent<Rigidbody2D>().isKinematic = true;
        sidekickPreviewGO.transform.localScale = new Vector3(1, 1, 1);
        sidekickPreviewGO.transform.position = new Vector3(previewSidekick.transform.position.x, previewSidekick.transform.position.y, -20);
        sidekickPreviewGO.GetComponent<LayerManager>().SetOrderByZCoordinate();
        textSpecialName.text = selectedHero.Special.Name;
    }
    void ChangePageLook(Vector2 imagesSize, GameObject page)
    {
        HeroPageComponent currentHeroPage = page.GetComponent<HeroPageComponent>();
        currentHeroPage.ImagePlaceholder.transform.localScale = imagesSize;
        currentHeroPage.ImageSidekick.transform.localScale = imagesSize;
        currentHeroPage.ImageLocked.transform.localScale = imagesSize;

    }

    }
