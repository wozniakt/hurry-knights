﻿using UnityEngine;
using System.Collections;
using TMPro;

public class HeroListaDataNameTextController : MonoBehaviour
{

    public void ChangeText(string text)
    {
        GetComponent<TextMeshProUGUI>().text = text;
    }
}
