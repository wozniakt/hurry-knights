﻿
using TMPro;
using UnityEngine;

public class HeroDescTextController : MonoBehaviour
{
    public void ChangeText(string text)
    {
        GetComponent<TextMeshProUGUI>().text = text;
    }
}

