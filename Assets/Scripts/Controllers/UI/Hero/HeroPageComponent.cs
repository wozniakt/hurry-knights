﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;

public class HeroPageComponent : MonoBehaviour
{
    public Image ImageHero, ImageSidekick, ImageLocked;
    public GameObject ImagePlaceholder;
    public TextMeshProUGUI TextMeshLocked;
    public GameObject PreviewHeroSmall;
    public int Index;

    public void ChangePage()
    {
        GlobalEventsManager.instance.TriggerEvent("ChangePage", new Hashtable { { "PageIndex", Index } });
    }
    
}
