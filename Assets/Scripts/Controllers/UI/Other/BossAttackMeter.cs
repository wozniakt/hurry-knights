﻿using UnityEngine;
using System.Collections;

public class BossAttackMeter : MonoBehaviour
{
    [SerializeField] float currentFill;
    [SerializeField] GameObject btnGroupMeter;
    [SerializeField] RectTransform pointer;
    [SerializeField] RectTransform fillMeterWin, fillMeterLost;
    Vector3[] fillMeterWinCorners, fillMeterLostCorners;
    Vector3 cornerLeftWinUp, cornerRightWinUp, cornerLeftLostUp, cornerRightLostUp;
    [SerializeField] float speed=5;
    // Use this for initialization
    void Start()
    {
        GlobalEventsManager.instance.StartListening("BossActive", Init);
        currentFill = 0;
        Time.timeScale = 1;
        GlobalEventsManager.instance.StartListening("BossAttackCheck", OnBossAttackCheck);
        fillMeterWinCorners = GetCorners(fillMeterWin);
        cornerLeftWinUp = fillMeterWinCorners[1];
        cornerRightWinUp = fillMeterWinCorners[3];

        fillMeterLostCorners = GetCorners(fillMeterLost);
        cornerLeftLostUp = fillMeterLostCorners[1];
        cornerRightLostUp = fillMeterLostCorners[3];
    }

    private void Init(Hashtable eventParams)
    {
        SetActiveMeter(true);
        currentFill = 0;
        Time.timeScale = 1;
        pointer.position = new Vector3(cornerLeftLostUp.x, cornerLeftLostUp.y, pointer.position.z);
    }
    void SetActiveMeter(bool active)
    {
        btnGroupMeter.SetActive(active);
    }
    void OnBossAttackCheck(Hashtable eventParams)
    {
        FactorySingletons.Instance.GameStateMan.CurrentGameState = GameStateManager.GameState.BossAttackCheck;
    }
    private void OnEnable()
    {
        currentFill = 0;
        Time.timeScale = 1;
        pointer.position = new Vector3(cornerLeftLostUp.x, cornerLeftLostUp.y, pointer.position.z);
    }
    // Update is called once per frame
    void Update()
    {

        if (FactorySingletons.Instance.GameStateMan.CurrentGameState == GameStateManager.GameState.BossActive)
        {
            if ((pointer.position.x >= cornerRightWinUp.x))
            {
                Time.timeScale = 1;
                FactorySingletons.Instance.GameStateMan.ChangeGameState(GameStateManager.GameState.Lost);
                SetActiveMeter(false);
                return;
            }

            Time.timeScale = 0.0003f;

            currentFill +=  speed;

            pointer.position =new Vector3((cornerLeftLostUp.x+ currentFill), cornerLeftLostUp.y-0.3f, pointer.position.z);
            //hero lost


        }

        if (FactorySingletons.Instance.GameStateMan.CurrentGameState == GameStateManager.GameState.BossAttackCheck)
        {
            Time.timeScale = 1;
            //hero lost
            if ((pointer.position.x >= cornerLeftLostUp.x) && (pointer.position.x <= cornerRightLostUp.x) || (pointer.position.x > cornerRightWinUp.x))
            {
            
                FactorySingletons.Instance.GameStateMan.ChangeGameState( GameStateManager.GameState.Lost);
                SetActiveMeter(false);
            }

            //hero win
            if ((pointer.position.x >= cornerRightLostUp.x) && (pointer.position.x <= cornerRightWinUp.x))
            {
           
                GlobalEventsManager.instance.TriggerEvent("HeroAttack");
                FactorySingletons.Instance.GameStateMan.ChangeGameState ( GameStateManager.GameState.Win);
                SetActiveMeter(false);
            }

        }

        }
    Vector3[] GetCorners(RectTransform rectTransform)
    {
        Vector3[] corners = new Vector3[4];
        rectTransform.GetComponent<RectTransform>().GetWorldCorners(corners);
        return corners;

    }
}
