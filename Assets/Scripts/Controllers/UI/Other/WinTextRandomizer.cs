﻿using UnityEngine;
using System.Collections;
using TMPro;

public class WinTextRandomizer : MonoBehaviour
{
    [SerializeField] TextsScrObj textsScrObj;
    [SerializeField] TextMeshProUGUI textMeshProUgui;
    // Use this for initialization
    private void Start()
    {
        GlobalEventsManager.instance.StartListening("GameStateChanged", ChangeText);
        if (textMeshProUgui==null)
        {
            textMeshProUgui=GetComponent<TextMeshProUGUI>();
        }
    }

    void ChangeText(Hashtable eventParams)
    {
        GameStateManager.GameState gameState = (GameStateManager.GameState)eventParams["GameState"];
        if (gameState != GameStateManager.GameState.Win) {
            textMeshProUgui.text = textsScrObj.GetRandomText();
        }

    }
}
