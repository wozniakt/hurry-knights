﻿using UnityEngine;
using System.Collections;
using TMPro;

public class SpecialTextController : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI textMeshProUgui;
    // Use this for initialization
    private void Start()
    {
        GlobalEventsManager.instance.StartListening("SpecialActivated", ChangeText);
        GlobalEventsManager.instance.StartListening("GameStateChanged", ChangeText);

        if (textMeshProUgui == null)
        {
            textMeshProUgui = GetComponent<TextMeshProUGUI>();
        }
    }

    void ChangeText(Hashtable eventParams)
    {
        string textToShow;
        textToShow = (string)eventParams["TextToShow"];
        if (eventParams.ContainsKey("GameState"))
        {
            GameStateManager.GameState gameState = (GameStateManager.GameState)eventParams["GameState"];
            if (gameState == GameStateManager.GameState.Win)
            {
                textToShow = "Level Completed";
            }
        }
            
        textMeshProUgui.text = textToShow;
    }
}
