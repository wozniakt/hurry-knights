﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using UI.Pagination;
using System.Collections;
using TMPro;

public class SponsorsScrollController : MonoBehaviour
{
    SponsorsListData CurrentHeroesListData;

    [SerializeField] PagedRect pagedRectSponsors;
    [SerializeField] PagedRect pagedRectHeroes;
    [SerializeField] TextMeshProUGUI textSpecialName;
    [SerializeField] TextMeshProUGUI textSpecialUnlockValue;
    [SerializeField] SponsorsList sponsors;

   public void OnShopPanelVisible()
    {

        if (pagedRectSponsors.Pages.Count == sponsors.SponsorsListData.Count)
        {
            pagedRectSponsors.UpdatePages();
            return;
        }

        StartCoroutine(PopulateScroll());
    }
   
    IEnumerator PopulateScroll()
    {
        Page pageToSelect = new Page();
        foreach (SponsorsListData heroesListData in sponsors.SponsorsListData)
        {
            Page newPage = pagedRectSponsors.AddPageUsingTemplate();
            newPage.GetComponent<HeroesListDataContainer>().Data = heroesListData;
            newPage.TextMeshPage.text = heroesListData.SponsorName;
            if (!heroesListData.Special.Unlocked)
            {
                textSpecialUnlockValue.enabled = true;
                textSpecialUnlockValue.text = heroesListData.Special.unlockValue.ToString();
            }
            else
            {
              //  textSpecialUnlockValue.enabled = false;
                textSpecialUnlockValue.text = "Unlocked";
            }
            
            textSpecialName.text = heroesListData.Special.Name;
            if (FactorySingletons.Instance.PlayerDataManager.CurrentPlayerData.CurrentSponsorListData == heroesListData)
            {
                pageToSelect = newPage;

            }
          //  pagedRectSponsors.UpdatePages();
        }
        //yield return new WaitForEndOfFrame();
        //pagedRectSponsors.SetCurrentPage(pageToSelect.PageNumber);
        //pagedRectSponsors.DefaultPage = (pageToSelect.PageNumber);
        //yield return new WaitForEndOfFrame();
        //pagedRectSponsors.UpdatePages();
        //pagedRectSponsors.UpdateDisplay();

        pagedRectSponsors.PageChangedEvent.AddListener(delegate
        {
           
            FactorySingletons.Instance.PlayerDataManager.CurrentPlayerData.CurrentSponsorListData = sponsors.SponsorsListData[pagedRectSponsors.CurrentPage-1];
            CurrentHeroesListData = pagedRectSponsors.GetCurrentPage().GetComponent<HeroesListDataContainer>().Data;
            if (!CurrentHeroesListData.Special.Unlocked)
            {
                textSpecialUnlockValue.enabled = true;
                textSpecialUnlockValue.text = CurrentHeroesListData.Special.unlockValue.ToString();
            }
            else
            {
               // textSpecialUnlockValue.enabled = false;
                textSpecialUnlockValue.text = "Unlocked";
            }
            textSpecialName.text = CurrentHeroesListData.Special.Name;
            GlobalEventsManager.instance.TriggerEvent("CurrentHeroesListUpdated",new Hashtable { { "CurrentHeroesListData", CurrentHeroesListData } });
            pagedRectHeroes.GetComponent<ScrollHeroesController>().PopulateHeroesScrollWithCurrentHeroesList(CurrentHeroesListData);

        });
        yield return new WaitForEndOfFrame();
        pagedRectSponsors.SetCurrentPage(pageToSelect.PageNumber);
        pagedRectSponsors.DefaultPage = (pageToSelect.PageNumber);
        yield return new WaitForSeconds(1f);
        pagedRectSponsors.UpdatePages();
        pagedRectSponsors.UpdateDisplay();
    }


}
