﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SponsorChangeControler : MonoBehaviour
{
    public SponsorsList sponsors;
    // public List<SponsorData> sponsors;
    SponsorsListData currentHeroesListData;
    // Use this for initialization

    private void Start()
    {
        currentHeroesListData = sponsors.SponsorsListData[0];
        GlobalEventsManager.instance.TriggerEvent("CurrentHeroesListUpdated", new Hashtable { { "CurrentHeroesListData", currentHeroesListData } });
    }

    public void NextSponsor()
    {
        ChangeSponsorIndex(+1);
    }

    public void PreviousSponsor()
    {
        ChangeSponsorIndex(-1);
    }

    void ChangeSponsorIndex(int increase)
    {
        //todo musthave scroll 
        Debug.Log("Before sponsors.HeroesListData.IndexOf(currentHeroesListData) : " + sponsors.SponsorsListData.IndexOf(currentHeroesListData));
        if (sponsors.SponsorsListData.IndexOf(currentHeroesListData) ==0 && increase<0)
        {
            currentHeroesListData = sponsors.SponsorsListData[sponsors.SponsorsListData.Count];
        }
        else if (sponsors.SponsorsListData.IndexOf(currentHeroesListData) == sponsors.SponsorsListData.Count && increase > 0)
        {
            currentHeroesListData = sponsors.SponsorsListData[0];
        }
        else
        {
            currentHeroesListData = sponsors.SponsorsListData[sponsors.SponsorsListData.IndexOf(currentHeroesListData) + increase];
        }
        Debug.Log("After sponsors.HeroesListData.IndexOf(currentHeroesListData) : " + sponsors.SponsorsListData.IndexOf(currentHeroesListData));
        UpdateSponsor();
    }
    // Update is called once per frame
    void UpdateSponsor()
    {
        GlobalEventsManager.instance.TriggerEvent("CurrentHeroesListUpdated", new Hashtable { { "CurrentHeroesListData", currentHeroesListData } });
    }
}
