﻿using UnityEngine;
using System.Collections;

public class ItemsPanelController : MonoBehaviour
{

    [SerializeField] GameObject ItemsPanel;
    [SerializeField] GameObject MenuButtonsGroup;
    // Use this for initialization
    public void ShowHideItems()
    {
        FactorySingletons.Instance.GameStateMan.ChangeGameState(GameStateManager.GameState.Menu);
        ItemsPanel.SetActive(!ItemsPanel.activeSelf);
        MenuButtonsGroup.SetActive(!MenuButtonsGroup.activeSelf);
    }
}
