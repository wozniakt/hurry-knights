﻿//using UnityEngine;
//using System.Collections.Generic;
//using UnityEngine.UI;
//using UI.Pagination;

//public class ScrollBackgroundsController : MonoBehaviour
//{
//    List<Background> Backgrounds = new List<Background>();

//    [SerializeField] PagedRect pagedRect;
//    [SerializeField] BackgroundDescTextController BackgroundDescText;


//    private void Start()
//    {
//        Backgrounds = FactorySingletons.Instance.GlobalDataContainer.BackgroundsListData.backgrounds;
//        pagedRect.PageChangedEvent.AddListener(delegate
//        {

//            Background selectedBg = pagedRect.GetCurrentPage().GetComponent<BackgroundData>().Data;
//            BackgroundDescText.ChangeText(selectedBg.Name);
//            if (selectedBg.Unlocked)
//            {
//                FactorySingletons.Instance.PlayerDataManager.CurrentPlayerData.CurrentBackground = selectedBg;
//            }
           

//        });
//        ShowListBackgrounds();
//    }

//    private void OnEnable()
//    {
//        ShowListBackgrounds();
//    }

//    void RefreshBackgrounds()
//    {

//        pagedRect.UpdatePages();
//        foreach (Page page in pagedRect.Pages)
//        {
//            Background Background = page.GetComponent<BackgroundData>().Data;
//            if (!Background.Unlocked)
//            {
//                //page.ImagePage.color = Color.black;
//                page.ImageLockedPage.enabled = true;
//            }
//            else
//            {
//                //page.ImagePage.color = Color.white;
//                page.ImageLockedPage.enabled = false;
//            }

//        }
//    }
//    System.Collections.IEnumerator PopulateScroll()
//    {
//        Page pageToSelect = new Page();
//        foreach (Background bg in Backgrounds)
//        {

//            Page newPage = pagedRect.AddPageUsingTemplate();
//            newPage.GetComponent<BackgroundData>().Data = bg;

//            newPage.ImagePage.sprite = bg.spriteImg;


//            if (!bg.Unlocked)
//            {
//                //newPage.ImagePage.color = Color.black;
//                newPage.ImageLockedPage.enabled = true;
//            }
//            if (FactorySingletons.Instance.PlayerDataManager.CurrentPlayerData.CurrentBackground == bg)
//            {
//                pageToSelect = newPage;

//            }
//            pagedRect.UpdatePages();
//        }
//        yield return new WaitForSeconds(0.1f);
//        pagedRect.SetCurrentPage(pageToSelect.PageNumber);
//        pagedRect.DefaultPage = (pageToSelect.PageNumber);
//        yield return new WaitForSeconds(0.1f);
//        pagedRect.UpdatePages();
//        Background selectedBg = pagedRect.GetCurrentPage().GetComponent<BackgroundData>().Data;
//        BackgroundDescText.ChangeText(selectedBg.Name);
//    } 

//    // Update is called once per frame
//    public void ShowListBackgrounds()
//    {
//        Backgrounds = FactorySingletons.Instance.GlobalDataContainer.BackgroundsListData.backgrounds;
//        if (pagedRect.Pages.Count == Backgrounds.Count)
//        {
//            RefreshBackgrounds();
//            return;
//        }
//       StartCoroutine( PopulateScroll());
//    }
//}
