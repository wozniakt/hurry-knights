﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UiAnimHeroesManager : MonoBehaviour
{
    [SerializeField] List<RectTransform> HeroUiAnimPrefabs, EnemyUiAnimPrefabs;
    [SerializeField] RectTransform heroesPlaceholder, enemiesPlaceholder;
    int currentCharsToGenerate;
    [SerializeField] float minSpeed = 600, maxSpeed = 1000;
    int rndHeroesToGenerate;
    [SerializeField] float timerHeroes, timerEnemies;
    [SerializeField] float delayTime = 4;
    // Use this for initialization
    void Start()
    {
        //SpawnRandomHeroes();
    }

    // Update is called once per frame
    IEnumerator SpawnRandomChars(RectTransform placeholder, int minCharsToGenerate, List<RectTransform> charsPrefabs, int direction)
    {
        rndHeroesToGenerate = Random.Range(1, 3);
        currentCharsToGenerate = Mathf.Min(currentCharsToGenerate + rndHeroesToGenerate, 10);
        for (int i = 0; i < charsPrefabs.Count; i++)
        {

            for (int j = 0; j < currentCharsToGenerate; j++)
            {
                yield return new WaitForSeconds(Random.Range(1, 4));
                GameObject charUi = FactorySingletons.Instance.PoolManager.SpawnGoInstance(charsPrefabs[i].gameObject);
                RectTransform heroUiRectTransform = charUi.GetComponent<RectTransform>();
                //RectTransform placeholder = RandomizePlaceHolder();
                heroUiRectTransform.SetParent ( placeholder.GetComponent<RectTransform>());
                heroUiRectTransform.position = placeholder.position;
                heroUiRectTransform.localScale = new Vector3(direction*placeholder.position.z, placeholder.position.z, 1);
                charUi.SetActive(true);
                charUi.GetComponent<UiANimMoveComponent>().IsMoving = true;
                charUi.GetComponent<UiANimMoveComponent>().Direction = direction;
                charUi.GetComponent<UiANimMoveComponent>().Speed = Random.Range(minSpeed, maxSpeed);
            }

        }
    }


    //RectTransform RandomizePlaceHolder()
    //{
    //  return  placeholders[Random.Range(0,placeholders.Count)];
    //}
    private void Update()
    {
        timerHeroes     += Time.deltaTime;
        if (timerHeroes > delayTime)
        {
            StartCoroutine(SpawnRandomChars(heroesPlaceholder, currentCharsToGenerate, HeroUiAnimPrefabs, 1));
            timerHeroes = 0;
            delayTime += 5;
        }

        timerEnemies += Time.deltaTime;
        if (timerEnemies > delayTime)
        {
           StartCoroutine( SpawnRandomChars(enemiesPlaceholder, currentCharsToGenerate, EnemyUiAnimPrefabs, -1));
            timerEnemies = 0;
            delayTime += 5;
        }

    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }
}
