﻿using UnityEngine;
using System.Collections;

public class UiANimMoveComponent : MonoBehaviour
{
    RectTransform rect;
    public bool IsMoving;
    public float Speed = 1;
    public float Direction = 1;
    // Use this for initialization
    void Start()
    {
        rect = this.GetComponent<RectTransform>();
    }

    // Update is called once per frame
    void Update()
    {
        if (IsMoving)
        {
            rect.Translate(Direction * Speed * Time.deltaTime, 0, 0);
        }
        
    }
}
