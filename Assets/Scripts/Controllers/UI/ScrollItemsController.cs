﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using UI.Pagination;

public class ScrollItemsController : MonoBehaviour
{
    List<Item> items = new List<Item>();


    //[SerializeField] HorizontalScrollSnap HorizontalScrollSnapBgs;
    [SerializeField] PagedRect pagedRect;
    [SerializeField] ItemDescTextController itemNameText;
    [SerializeField] ItemDescTextController itemDescText;
    // Use this for initialization

    private void Start()
    {
        items = FactorySingletons.Instance.GlobalDataContainer.ItemsListData.items;
        pagedRect.PageChangedEvent.AddListener(delegate 
        {
            itemNameText.ChangeText(pagedRect.GetCurrentPage().GetComponent<ItemData>().Data.Name);
            itemDescText.ChangeText(pagedRect.GetCurrentPage().GetComponent<ItemData>().Data.Description);
        });
        ShowListItems();
    }

    private void OnEnable()
    {
        ShowListItems();
    }

    void RefreshItems()
    {

        pagedRect.UpdatePages();
        foreach (Page page in pagedRect.Pages)
        {
            Item item = page.GetComponent<ItemData>().Data;
            if (!item.Unlocked)
            {
               page.ImagePage.color = Color.black;
               page.ImageLockedPage.enabled = true;
            }
            else
            {
                page.ImagePage.color = Color.white;
                page.ImageLockedPage.enabled = false;
            }

        }
    }
    void PopulateScroll()
    {
        foreach (Item item in items)
        {

            Page newPage= pagedRect.AddPageUsingTemplate();
            newPage.GetComponent<ItemData>().Data = item;

            if (item.spriteImg == null)
            {
                newPage.ImagePage.sprite = item.prefab.GetComponentInChildren<SpriteRenderer>().sprite;
            }
            else
            {
                newPage.GetComponentInChildren<Image>().sprite = item.spriteImg;
                
            }

            if (!item.Unlocked)
            {
                newPage.ImagePage.color = Color.black;
                newPage.ImageLockedPage.enabled = true;
            }

            pagedRect.UpdatePages();
            itemNameText.ChangeText(pagedRect.GetCurrentPage().GetComponent<ItemData>().Data.Name);
            itemDescText.ChangeText(pagedRect.GetCurrentPage().GetComponent<ItemData>().Data.Description);
        }
    }

    // Update is called once per frame
    public void ShowListItems()
    {
        items = FactorySingletons.Instance.GlobalDataContainer.ItemsListData.items;
        if (pagedRect.Pages.Count == items.Count)
        {
            RefreshItems();
            return;
        }
        PopulateScroll();
    }
}
