﻿
using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class SelectToggleCartController : MonoBehaviour
{
    [SerializeField] Toggle toggle;
    [SerializeField] HeroData dataCart;
    // Use this for initialization
    void Start()
    {
        toggle.onValueChanged.AddListener(delegate {
            ToggleValueChanged(toggle);
        });
    }

    // Update is called once per frame
    void ToggleValueChanged(Toggle change)
    {
        if (toggle.isOn == true)
        {
            FactorySingletons.Instance.PlayerDataManager.CurrentPlayerData.CurrentHero = dataCart.Data;
        }
    }
}
