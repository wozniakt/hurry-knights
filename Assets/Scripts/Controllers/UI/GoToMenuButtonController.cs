﻿using UnityEngine;
using System.Collections;

public class GoToMenuButtonController : MonoBehaviour
{

    public void GoToMenu()
    {
        FactorySingletons.Instance.PoolManager.ResetPooledList();
        FactorySingletons.Instance.PlayerDataManager.SavePlayerData();
        FactorySingletons.Instance.PlayerDataManager.LoadPlayerData();
        FactorySingletons.Instance.PlayerDataManager.ResetPlayerHp();
        FactorySingletons.Instance.GameStateMan.LoadGameScene(GameStateManager.GameState.Menu);
    }
}
