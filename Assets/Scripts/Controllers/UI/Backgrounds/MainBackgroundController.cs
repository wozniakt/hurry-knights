﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MainBackgroundController : MonoBehaviour
{
    public List<BackgoundChangeController> BackgoundChangeControllers;
    List<BackgroundElements> backgroundsElements;

    void Start()
    {
        backgroundsElements = FactorySingletons.Instance.PlayerDataManager.CurrentPlayerData.BackgroundsElements;
        GlobalEventsManager.instance.StartListening("UpdateBackground", OnUpdateBackground);
    }

    void OnUpdateBackground(Hashtable eventParams)
    {
        BackgroundElements backgroundElements = new BackgroundElements();
        if (eventParams != null)
        {
            if (eventParams.ContainsKey("BackgroundElementsIndex"))
            {
                int bgIndex = (int)eventParams["BackgroundElementsIndex"];
                backgroundElements = backgroundsElements[bgIndex];
            }
        }
        else
        {
            backgroundElements = GetRandomBackground();
        }
        
        foreach (BackgoundChangeController backgoundChangeController in BackgoundChangeControllers)
        {
    
            backgoundChangeController.GetComponent<MeshRenderer>().materials[0].mainTexture = backgroundElements.backgroundTextures[backgoundChangeController.BackgroundIndex];
        }

    }
    BackgroundElements GetRandomBackground()
    {
        int rnd = UnityEngine.Random.Range(0, backgroundsElements.Count);
        if (FactorySingletons.Instance.PlayerDataManager.CurrentPlayerData.CurrentBgIndex==rnd)
        {
            rnd = Mathf.Min(rnd+1, backgroundsElements.Count);
        }
        if (FactorySingletons.Instance.PlayerDataManager.CurrentPlayerData.CurrentBgIndex == rnd)
        {
            rnd = Mathf.Max(rnd - 1, 0);
        }
        PlayerPrefs.SetInt("CurrentBgIndex", rnd);
        return backgroundsElements[rnd];
    }


}
