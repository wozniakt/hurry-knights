﻿using UnityEngine;
using System.Collections;

public class StartGameButtonController : MonoBehaviour
{

    // Use this for initialization
    public void StartGame()
    {
        FactorySingletons.Instance.PoolManager.ResetPooledList();
        FactorySingletons.Instance.PlayerDataManager.SavePlayerData();
        FactorySingletons.Instance.PlayerDataManager.LoadPlayerData();
        FactorySingletons.Instance.PlayerDataManager.ResetPlayerHp();
        FactorySingletons.Instance.PlayerDataManager.CurrentPlayerData.CurrentPoints = 0;
        FactorySingletons.Instance.GameStateMan.LoadGameScene(GameStateManager.GameState.GameOn);
      //  FactorySingletons.Instance.GameStateMan.ChangeGameState(GameStateManager.GameState.GameOn);
    }

}
