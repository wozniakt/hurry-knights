﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TMPro;
using UnityEngine;

public class ItemDescTextController:MonoBehaviour
    {
        public void ChangeText(string text)
        {
        GetComponent<TextMeshProUGUI>().text = text;
        }
    }

