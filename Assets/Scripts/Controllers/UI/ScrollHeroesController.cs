﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using UI.Pagination;
using System.Collections;
using TMPro;

public class ScrollHeroesController : MonoBehaviour
{
    [SerializeField] Button ConfirmButton;
    List<Hero> Heroes = new List<Hero>();
    Hero selectedHero;
    //[SerializeField] HorizontalScrollSnap HorizontalScrollSnapBgs;
        [SerializeField] PagedRect pagedRect;
    //[SerializeField] HeroNameTextController HeroNameCtrl;
    //[SerializeField] HeroDescTextController HeroDescCtrl;
    //[SerializeField] HeroSpecialTextController HeroSPecialCtrl;

    [SerializeField] TextMeshProUGUI textHeroName;
    [SerializeField] TextMeshProUGUI textHeroDesc;
    [SerializeField] TextMeshProUGUI textHeroSpecialName;
    [SerializeField] TextMeshProUGUI textHeroSpecialDesc;
    // Use this for initialization



   public  void PopulateHeroesScrollWithCurrentHeroesList(SponsorsListData currentHeroesListData)
    {

        StartCoroutine(PopulateScroll( currentHeroesListData.Heroes));


    }


    IEnumerator PopulateScroll(List<Hero> heroes)
    {
        //pagedRect.PageChangedEvent.RemoveAllListeners();
        foreach (Page page in pagedRect.Pages)
        {
            Destroy(page.gameObject);
        }
        pagedRect.Pages.Clear();
        yield return new WaitForEndOfFrame();
        pagedRect.UpdatePages();
        yield return new WaitForEndOfFrame();

        Page pageToSelect = new Page();
        foreach (Hero hero in heroes)
        {
            Page newPage = pagedRect.AddPageUsingTemplate();
            newPage.GetComponent<HeroData>().Data = hero;


            newPage.ImagePage.sprite = hero.SpriteImg;
            newPage.ImagePage2.sprite = hero.SpriteImgSidekick;
            if (!hero.Unlocked)
            {
                // newPage.ImagePage.color = Color.black;
                newPage.ImageLockedPage.enabled = true;
                newPage.TextMeshLocked.enabled = true;
                newPage.TextMeshLocked.text = hero.UnlockValue.ToString();
            }
            else
            {
                newPage.ImageLockedPage.enabled = false;
                newPage.TextMeshLocked.enabled = false;
            }

  
            if (FactorySingletons.Instance.PlayerDataManager.CurrentPlayerData.CurrentHero == hero || pageToSelect==null)
            {
                pageToSelect = newPage;

            }
            pagedRect.UpdatePages();
        }
        if (pageToSelect.PageNumber<=0)
        {
            pageToSelect.PageNumber = 0;
        }
        //yield return new WaitForSeconds(1f);
        pagedRect.UpdatePages();
        pagedRect.SetCurrentPage(pageToSelect.PageNumber);
        pagedRect.DefaultPage = (pageToSelect.PageNumber);
        //yield return new WaitForSeconds(1f);
        pagedRect.UpdateDisplay();
        pagedRect.UpdatePages();
    }

    public void SelectCurrentHero()
    {
        selectedHero = pagedRect.GetCurrentPage().gameObject.GetComponent<HeroData>().Data;

        textHeroName.text = selectedHero.Name;
        textHeroDesc.text = "Unlock points: "+selectedHero.UnlockValue.ToString();
        //textHeroSpecialName.text = (selectedHero.Special.Name);
        //textHeroSpecialDesc.text = (selectedHero.Special.Description);

        if (selectedHero.Unlocked)
        {
            ConfirmButton.enabled = true;
            FactorySingletons.Instance.PlayerDataManager.CurrentPlayerData.CurrentHero = selectedHero;
        }
        else
        {
            ConfirmButton.enabled = false;
        }
        //pagedRect.SetCurrentPage(pagedRect.GetCurrentPage().PageNumber);
        //pagedRect.DefaultPage = (pagedRect.GetCurrentPage().PageNumber);

        pagedRect.UpdatePages();
    }
}
