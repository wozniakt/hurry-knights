﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;

public class MainUiController : MonoBehaviour
{
    [SerializeField] GameObject PointsTextGo;
    //[SerializeField] GameObject ParallaxPlaceHolder;
    [SerializeField] GameObject HpImageGo;
    [SerializeField] Image HpImage;
    [SerializeField] GameObject PanelMenu;
    [SerializeField] GameObject PanelShop;
    [SerializeField] GameObject PanelShopCanvasBg;
    [SerializeField] GameObject PanelShopCanvasParticle;
    [SerializeField] GameObject PanelGameOver;
    [SerializeField] GameObject PanelGameHud, BackgroundSaturationMask;
    [SerializeField] GameObject PanelGameWin;
    [SerializeField] TextMeshProUGUI PointsText;
    [SerializeField] TextMeshProUGUI HighscoreText;
    [SerializeField] TextMeshProUGUI HighscoreTextInMenu;
    [SerializeField] TextMeshProUGUI LevelTextInHud;

    [SerializeField] HeroesAllScrollController heroesAllScrollController;
    // Use this for initialization
    void Start()
    {
        GlobalEventsManager.instance.StartListening("CurrentHeroesListUpdated", OnSponsorUpdate);
        GlobalEventsManager.instance.StartListening("PlayerDataChanged", OnPlayerDataChange);
        GlobalEventsManager.instance.StartListening("GameStateChanged", OnGameStateChange);
        PlayerData playerData=(FactorySingletons.Instance.PlayerDataManager.CurrentPlayerData);
        PointsText.text ="COINTS "+ playerData.CurrentPoints.ToString();
        HpImage.fillAmount = (float)playerData.CurrentHp / (float)playerData.MaxHp;
    }

    void OnGameStateChange(Hashtable eventParams)
    {
        GameStateManager.GameState gameState =(GameStateManager.GameState) eventParams["GameState"];

        PanelMenu.SetActive(false);
        //PanelShop.SetActive(false);
        PanelGameOver.SetActive(false);
        PanelGameHud.SetActive(false);
        BackgroundSaturationMask.SetActive(false);
        //  PanelGameWin.SetActive(false);

        switch (gameState)
        {


            case (GameStateManager.GameState.Menu):
            //   ParallaxPlaceHolder.SetActive(false);
                PanelMenu.SetActive(true);
                PanelShop.SetActive(false);
                PanelShopCanvasBg.SetActive(false);
                PanelShopCanvasParticle.SetActive(false);
                //PanelGameOver.SetActive(false);
                //PanelGameHud.SetActive(false);
                // PanelGameWin.SetActive(false);
                break;
            case (GameStateManager.GameState.Shop):
              //  ParallaxPlaceHolder.SetActive(false);
                PanelShop.SetActive(true);
                PanelShopCanvasBg.SetActive(true);
                PanelShopCanvasParticle.SetActive(true);
                heroesAllScrollController.OnShopPanelVisible();
                //PanelGameOver.SetActive(false);
                //PanelGameHud.SetActive(false);
                // PanelGameWin.SetActive(false);
                break;
            case (GameStateManager.GameState.ShopWorkaround):
                //  ParallaxPlaceHolder.SetActive(false);
                PanelMenu.SetActive(true);
                PanelMenu.SetActive(false);
                PanelShop.SetActive(true);
                PanelShopCanvasBg.SetActive(true);
                PanelShopCanvasParticle.SetActive(true);
                heroesAllScrollController.OnShopPanelVisible();
                //PanelGameOver.SetActive(false);
                //PanelGameHud.SetActive(false);
                // PanelGameWin.SetActive(false);
                break;
            case (GameStateManager.GameState.GameOn):
                //   ParallaxPlaceHolder.SetActive(true);
                //PanelMenu.SetActive(false);
                //PanelGameOver.SetActive(false);
                PanelShop.SetActive(false);
                PanelShopCanvasBg.SetActive(false);
                PanelShopCanvasParticle.SetActive(false);
                PanelGameHud.SetActive(true);
                BackgroundSaturationMask.SetActive(true);
                // PanelGameWin.SetActive(false);
                break;
            case (GameStateManager.GameState.Win):
                //  GamePlaceHolder.SetActive(false);
                PanelShop.SetActive(false);
                PanelShopCanvasBg.SetActive(false);
                PanelShopCanvasParticle.SetActive(false);
                PanelMenu.SetActive(false);
                PanelGameOver.SetActive(false);
                PanelGameHud.SetActive(false);
                Debug.Log("Boss lost from ui");
                // PanelGameWin.SetActive(true);
                break;
            case (GameStateManager.GameState.Lost):
 
                // GamePlaceHolder.SetActive(false);
                //PanelMenu.SetActive(false);
                PanelGameOver.SetActive(true);
                BackgroundSaturationMask.SetActive(true);
                //PanelGameHud.SetActive(false);
                // PanelGameWin.SetActive(false);
                break;
            case (GameStateManager.GameState.RestartGame):
                //      GamePlaceHolder.SetActive(false);
                //PanelMenu.SetActive(false);
                //PanelGameOver.SetActive(false);
                //PanelGameHud.SetActive(false);
                //  PanelGameWin.SetActive(false);
                PanelShop.SetActive(false);
                PanelShopCanvasBg.SetActive(false);
                PanelShopCanvasParticle.SetActive(false);
                break;
        }
    }
    void TestDebug()
    {
        if (PanelGameOver.activeSelf==false)
        {
            System.Diagnostics.Debugger.Break();
        }
    }
    // Update is called once per frame
    void OnPlayerDataChange(Hashtable eventParams)
    {
        PlayerData CurrentPlayerData = (PlayerData)eventParams["PlayerData"];
        PointsText.text = "COINTS " + CurrentPlayerData.CurrentPoints.ToString();
        HighscoreText.text = "HIGHSCORE: " + CurrentPlayerData.HiscorePoints.ToString();
        HighscoreTextInMenu.text = "HIGHSCORE: " + CurrentPlayerData.HiscorePoints.ToString();
        HpImage.fillAmount =(float) CurrentPlayerData.CurrentHp / (float) CurrentPlayerData.MaxHp;
        LevelTextInHud.text = "Level " + CurrentPlayerData.CurrentLevel;
    }
    void OnSponsorUpdate(Hashtable eventParams)
    {
       // HeroesListData CurrentSponsorData = (HeroesListData)eventParams["CurrentHeroesListData"];

       
    }
    private void OnDisable()
    {
        Debug.Log("Disable");
    }
}
