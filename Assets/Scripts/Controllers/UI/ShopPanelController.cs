﻿using UnityEngine;
using System.Collections;

public class ShopPanelController : MonoBehaviour
{
    [SerializeField] GameObject ShopPanel;
    [SerializeField] GameObject MenuButtonsGroup;
    // Use this for initialization
    public void ShowShop()
    {
        FactorySingletons.Instance.GameStateMan.ChangeGameState(GameStateManager.GameState.Shop);

    }
    public void HideShop()
    {
        FactorySingletons.Instance.GameStateMan.ChangeGameState(GameStateManager.GameState.Menu);

    }

}
