﻿using UnityEngine;
using System.Collections;


public class ItemStraightFallingController : MonoBehaviour
{
    IFalling fallingComponent;
    // Use this for initialization
   public void OnMouseDown()
    {
        if (FactorySingletons.Instance.GameStateMan.CurrentGameState==GameStateManager.GameState.GameOn)
        {
            fallingComponent.StartFalling();
        }
        
    }

    void Start()
    {
        fallingComponent = GetComponent<IFalling>();
    }
}
