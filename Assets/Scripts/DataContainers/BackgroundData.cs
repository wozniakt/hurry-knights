﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class BackgroundData : MonoBehaviour
{
    [SerializeField]
    public Background Data { get; set; }
 
}
