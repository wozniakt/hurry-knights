﻿

using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class ItemData : MonoBehaviour
{
    [SerializeField]
    public Item Data { get; set; }

}