﻿public interface ICharacterController
{
    void ChangeAnimationBool(string boolName, bool state);
    void ChangeAnimationTrigger(string triggerName);
}

