﻿using DG.Tweening;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


interface ITweenable
{
    void DoTweenAnim();
    void GetDoTweenAnim(Action<bool> callback);

}


